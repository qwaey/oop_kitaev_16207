#include <iostream>
#include <fstream>
#include <algorithm>
#include <exception>
#include <string>
#include <cstdio>
#include <vector>
#include <list>

using Key = std::string;

struct Value
{
	Value(unsigned age = 0, unsigned weight = 0)
		:age(age), weight(weight) {}
	unsigned age;
	unsigned weight;
};

class HashTable
{
private:

	using HTPair = std::pair< Key, Value >;
	using HTList = std::list< HTPair >;
	using HTVector = std::vector< HTList *>;

	static const size_t MAX_OCCUP = 75; //%
	static const size_t INIT_SIZE = 7;
	size_t curSize = INIT_SIZE;
	HTVector *table;
	size_t qElem;

	size_t hash(const Key &) const;
	size_t getNewTableSize(size_t);
	void copyVector(HTVector *, const HashTable &);
	void destroy(HTVector *, size_t);
	void resize();
	bool setIterIfContainsKey(HTList::iterator *, HTList *, const Key &) const;
	Value &ht_at(const Key &) const;

public:

	HashTable();
	HashTable(const HashTable &);
	HashTable(HashTable &&);
	~HashTable();

	// Exchange the values of two hash tables
	void swap(HashTable &);

	HashTable &operator=(const HashTable &);
	HashTable &operator=(HashTable &&);

	// Clears the container. Returns the table to the initial size
	void clear();

	// Deletes the element with the given key
	bool erase(const Key &);

	// Insert into container
	bool insert(const Key &, const Value &);

	// Check for the value of a given key
	bool contains(const Key &);

	//Returns the value by key.An unsafe method.If there is no key in the container, 
	//it inserts the value Value created by the constructor by default and returns a reference to it.
	Value &operator[](const Key &);

	// Returns the value by key
	Value &at(const Key &);
	const Value &at(const Key &) const;

	// Returns the size of the hash table (the number of elements in it)
	size_t size() const;

	// Returns true if the table is empty, otherwise false
	bool empty() const;

	// A pair of operators to check for hash table equality
	friend bool operator==(const HashTable &, const HashTable &);
	friend bool operator!=(const HashTable &, const HashTable &);
};