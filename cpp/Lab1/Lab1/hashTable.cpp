#include "hashTable.h"


HashTable::HashTable()
{
	table = new HTVector(INIT_SIZE);
	qElem = 0;
}




HashTable::HashTable(const HashTable &secTable)
{
	table = new HTVector(secTable.curSize);
	HashTable::copyVector(table, secTable);
	qElem = secTable.qElem;
}




HashTable::HashTable(HashTable&& secTable)
	: curSize(secTable.curSize)
	, table(secTable.table)
	, qElem(secTable.qElem)
{
	secTable.curSize = 0;
	secTable.table = nullptr;
	secTable.qElem = 0;
}




HashTable::~HashTable()
{
	HashTable::destroy(table, curSize);
	delete table;
}




size_t HashTable::hash(const Key &str) const
{
	return (std::hash<Key>()(str) % curSize);
}




size_t HashTable::size() const
{
	return qElem;
}




bool HashTable::empty() const
{
	return !qElem;
}




void HashTable::clear()
{
	HashTable::destroy(table, curSize);
	table->resize(INIT_SIZE);
	curSize = INIT_SIZE;
	qElem = 0;
}




Value& HashTable::ht_at(const Key &surname) const
{
	size_t index = HashTable::hash(surname);

	if (!(*table)[index])
	{
		throw (std::out_of_range(surname));
	}

	HashTable::HTList::iterator iter;

	if (setIterIfContainsKey(&iter, (*table)[index], surname))
	{
		return iter->second;
	}
	else
	{
		throw (std::out_of_range(surname));
	}
}




Value& HashTable::at(const Key &surname)
{
	return HashTable::ht_at(surname);
}




const Value& HashTable::at(const Key &surname) const
{
	return HashTable::ht_at(surname);
}




Value &HashTable::operator[](const Key &surname)
{
	size_t index = HashTable::hash(surname);

	// new list
	if (!(*table)[index])
	{
		HashTable::insert(surname, Value{ 0, 0 });

		//recounting the hash in case of a change in size
		index = HashTable::hash(surname);
	}

	// search list
	HTList::iterator iter;

	if (setIterIfContainsKey(&iter, (*table)[index], surname))
	{
		return iter->second;
	}
	else
	{
		(*table)[index]->push_back(std::make_pair(surname, Value{ 0, 0 }));
		return (*table)[index]->back().second;
	}
}




void HashTable::resize()
{
	size_t oldSize = curSize;
	curSize = getNewTableSize(curSize);

	HTVector *newTable = 0;

	try
	{
		newTable = new HTVector(curSize);
	}
	catch (std::bad_alloc &)
	{
		curSize = oldSize;
		throw;
	}

	for (size_t i = 0; i < oldSize; i++)
	{
		if (!(*table)[i])
		{
			continue;
		}

		for (HTList::iterator iter = ((*table)[i])->begin(); iter != ((*table)[i])->cend(); iter++)
		{
			size_t index = HashTable::hash(iter->first);

			// new list or insert
			try
			{
				if (!(*newTable)[index])
				{
					(*newTable)[index] = new HTList;
				}

				(*newTable)[index]->push_back(make_pair(iter->first, iter->second));
			}
			catch (std::bad_alloc &)
			{
				HashTable::destroy(newTable, curSize);
				delete newTable;
				curSize = oldSize;
				throw;
			}
		}
	}

	HashTable::destroy(table, oldSize);
	delete table;

	table = newTable;
}




bool HashTable::setIterIfContainsKey(HTList::iterator *iter, HTList *list, const Key &surname) const
{
	for ((*iter) = list->begin(); (*iter) != list->cend(); (*iter)++)
	{
		if (surname == (*iter)->first)
		{
			return true;
		}
	}

	return false;
}




HashTable &HashTable::operator=(const HashTable &secTable)
{
	if (this == &secTable)
	{
		return (*this);
	}

	HTVector *newTable = new HTVector(secTable.curSize);
	HashTable::copyVector(newTable, secTable);
	
	HashTable::destroy(table, curSize);
	delete table;

	qElem = secTable.qElem;
	curSize = secTable.curSize;
	table = newTable;

	return (*this);
}




HashTable &HashTable::operator=(HashTable&& secTable)
{
	if (this == &secTable)
	{
		return (*this);
	}

	HashTable::destroy(table, curSize);
	delete table;

	qElem = secTable.qElem;
	curSize = secTable.curSize;
	table = secTable.table;

	secTable.table = nullptr;
	secTable.curSize = 0;
	secTable.qElem = 0;

	return (*this);
}




size_t HashTable::getNewTableSize(size_t curNum)
{
	curNum *= 2;
	size_t i;

	while (true)
	{
		++curNum;

		for (i = 2; i <= curNum / 2; i++)
		{
			if (curNum % i == 0)
			{
				break;
			}
		}

		if (i > curNum / 2)
		{
			break;
		}
	}

	return curNum;
}




void HashTable::copyVector(HTVector *firstVect, const HashTable &secTable)
{
	(*firstVect).resize(secTable.curSize);

	for (size_t i = 0; i < secTable.curSize; i++)
	{
		if (!(*secTable.table)[i])
		{
			continue;
		}

		try
		{
			(*firstVect)[i] = new HTList;

			for (HTList::iterator iter = ((*secTable.table)[i])->begin(); iter != ((*secTable.table)[i])->cend(); iter++)
			{
				(*firstVect)[i]->push_back(make_pair(iter->first, iter->second));
			}
		}
		catch (std::bad_alloc &)
		{
			destroy(firstVect, secTable.curSize);
			delete firstVect;
			throw;
		}
	}
}




void HashTable::destroy(HTVector *Vect, size_t size)
{
	for (size_t i = 0; i < size; i++)
	{
		delete (*Vect)[i];
		(*Vect)[i] = 0;
	}
}




bool HashTable::erase(const Key &surname)
{
	size_t index = HashTable::hash(surname);

	if (!(*table)[index])
	{
		return false;
	}

	HTList::iterator iter;

	if (setIterIfContainsKey(&iter, (*table)[index], surname))
	{
		(*table)[index]->erase(iter);
		qElem--;
		return true;
	}
	else
	{
		return false;
	}
}




void HashTable::swap(HashTable& secTable)
{
	std::swap(table, secTable.table);
	std::swap(qElem, secTable.qElem);
	std::swap(curSize, secTable.curSize);
}




bool operator==(const HashTable &leftTable, const HashTable &rightTable)
{
	if (&leftTable == &rightTable)
	{
		return true;
	}

	if (leftTable.curSize != rightTable.curSize)
	{
		return false;
	}

	if (leftTable.qElem != rightTable.qElem)
	{
		return false;
	}

	for (size_t i = 0; i < leftTable.curSize; i++)
	{
		HashTable::HTList *Left = (*leftTable.table)[i];
		HashTable::HTList *Right = (*rightTable.table)[i];

		if (!Left != !Right) // compare the presence of pointers, not their meaning!
		{
			return false;
		}

		if (!(Left || Right))
		{
			continue;
		}

		if (Left->size() != Right->size())
		{
			return false;
		}

		HashTable::HTList::iterator iterLeft = Left->begin();
		HashTable::HTList::iterator iterRight = Right->begin();

		for (; iterLeft != Left->cend(); iterLeft++, iterRight++)
		{
			if (iterLeft->first != iterRight->first ||
				iterLeft->second.age != iterRight->second.age ||
				iterLeft->second.weight != iterRight->second.weight)
			{
				return false;
			}
		}
	}

	return true;
}




bool operator!=(const HashTable &leftTable, const HashTable &rightTable)
{
	return !(leftTable == rightTable);
}




bool HashTable::contains(const Key &surname)
{
	size_t index = HashTable::hash(surname);

	if (!(*table)[index])
	{
		return false;
	}

	HTList::iterator iter;
	return (setIterIfContainsKey(&iter, (*table)[index], surname));
}




bool HashTable::insert(const Key &surname, const Value &data)
{
	// resize?
	if (100 * (qElem + 1) / curSize > MAX_OCCUP)
	{
		HashTable::resize();
	}

	size_t index = HashTable::hash(surname);

	// new list
	if (!(*table)[index])
	{
		try
		{
			(*table)[index] = new HTList;
			(*table)[index]->push_back(make_pair(surname, Value{ data.age, data.weight }));
			++qElem;
		}
		catch (std::bad_alloc &)
		{
			throw;
		}

		return true; // successful insertion
	}


	// insert on list
	HTList::iterator iter;

	if (setIterIfContainsKey(&iter, (*table)[index], surname))
	{
		iter->second = Value{ data.age, data.weight };
	}
	else
	{
		(*table)[index]->push_back(make_pair(surname, Value{ data.age, data.weight }));
		qElem++;
	}

	return true; // successful insertion
}