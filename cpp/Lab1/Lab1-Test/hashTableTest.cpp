#include <gtest/gtest.h>
#include "../Lab1/hashTable.h"

class hashTableTest : public testing::Test
{
public:
	HashTable createTableTest()
	{
		HashTable temp;
		return temp;
	}
};


TEST_F(hashTableTest, construct)
{
	ASSERT_NO_THROW(HashTable temp);
}


TEST_F(hashTableTest, copyConstruct)
{
	HashTable temp1;
	ASSERT_NO_THROW(HashTable temp(temp1));
}


TEST_F(hashTableTest, insert)
{
	HashTable temp;

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE( temp.insert((name + std::to_string(i)), { 70, 19 }) );
	}
}


TEST_F(hashTableTest, empty)
{
	HashTable temp;
	ASSERT_TRUE(temp.empty());
	ASSERT_NO_THROW(temp.insert("Kitaev", { 70, 19 }));
	ASSERT_FALSE(temp.empty());
}


TEST_F(hashTableTest, contains)
{
	HashTable temp;

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp.insert((name + std::to_string(i)), { 70, 19 }));
	}

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp.contains(name + std::to_string(i)));
	}
}


TEST_F(hashTableTest, clear)
{
	HashTable temp;

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp.insert((name + std::to_string(i)), { 70, 19 }));
	}

	ASSERT_NO_THROW(temp.clear());
	ASSERT_TRUE(temp.empty());
}


TEST_F(hashTableTest, at)
{
	HashTable temp;
	int Max = 100;

	for (int i = 0; i < Max; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp.insert((name + std::to_string(i)), { 70, 19 }));
	}

	for (int i = 0; i < Max; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_NO_THROW( temp.at(name + std::to_string(i)) );
	}
}


TEST_F(hashTableTest, operator_square_sco)
{
	HashTable temp;
	int Max = 100;

	for (int i = 0; i < Max; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_NO_THROW(temp[name + std::to_string(i)]);
	}
}


TEST_F(hashTableTest, contain)
{
	HashTable temp1, temp2;
	
	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp1.insert((name + std::to_string(i)), { 70, 19 }));
	}

	ASSERT_NO_THROW(temp2 = temp1);

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp2.contains(name + std::to_string(i)));
	}
}


TEST_F(hashTableTest, operator_assign)
{
	ASSERT_NO_THROW(HashTable temp1 = createTableTest());
}


TEST_F(hashTableTest, operator_eq)
{
	HashTable temp1, temp2;

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		temp1.insert((name + std::to_string(i)), { 70, 19 });
		temp2.insert((name + std::to_string(i)), { 70, 19 });
	}

	ASSERT_TRUE(temp1 == temp2);
	ASSERT_FALSE(temp1 != temp2);
}


TEST_F(hashTableTest, erase)
{
	HashTable temp;

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		temp.insert((name + std::to_string(i)), { 70, 19 });
	}

	for (int i = 0; i < 100; i++)
	{
		std::string name = "Kitaev ";
		ASSERT_TRUE(temp.erase(name + std::to_string(i)));
	}
}


TEST_F(hashTableTest, swap)
{
	HashTable temp1, temp2;

	temp1.insert("Kitaev", {70, 19});

	ASSERT_NO_THROW(temp1.swap(temp2));

	ASSERT_TRUE(temp2.contains("Kitaev"));
}