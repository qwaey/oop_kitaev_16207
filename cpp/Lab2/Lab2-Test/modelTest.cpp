#include <gtest/gtest.h>
#include "../Lab2/cell.h"
#include "../Lab2/field.h"

class CellTest : public testing::Test
{

};

class fieldTest : public testing::Test
{

};


TEST_F(CellTest, cellConstruct)
{
	ASSERT_NO_THROW(model::Cell temp);
}

TEST_F(CellTest, cellConstruct2)
{
	ASSERT_NO_THROW(model::Cell temp(true));
	ASSERT_NO_THROW(model::Cell temp(false));
}

TEST_F(CellTest, cellCondition)
{
	model::Cell temp;

	ASSERT_NO_THROW(temp.setCond(true));
	ASSERT_TRUE(temp.condition());

	ASSERT_NO_THROW(temp.setCond('1'));
	ASSERT_TRUE(temp.condition());
}

TEST_F(CellTest, cellNeighbor)
{
	model::Cell temp;

	ASSERT_NO_THROW(temp.setNeighbor(4));
	ASSERT_EQ(4, temp.getNeighbor());
}




TEST_F(fieldTest, fieldConstruct)
{
	ASSERT_NO_THROW(model::Field temp);
}

TEST_F(fieldTest, fieldSize)
{
	model::Field temp;

	ASSERT_EQ(10, temp.getReferenceOnField().size());
	ASSERT_EQ(10, temp.getReferenceOnField()[0].size());
}

TEST_F(fieldTest, fieldSize2)
{
	model::Field temp;
	
	temp.updateCurSize(20, 20);

	ASSERT_EQ(20, temp.getReferenceOnField().size());
	ASSERT_EQ(20, temp.getReferenceOnField()[0].size());
}

TEST_F(fieldTest, fieldDef)
{
	model::Field temp;

	temp.updateCurSize(20, 20);
	temp.setDefaultField();

	temp.updateCurSize(50, 50);
	temp.loadDefaultField();

	ASSERT_EQ(50, temp.getReferenceOnField().size());
	ASSERT_EQ(50, temp.getReferenceOnField()[0].size());
}