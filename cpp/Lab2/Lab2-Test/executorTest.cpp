#include <gtest/gtest.h>
#include "../Lab2/executor.h"
#include "../Lab2/except.h"

class ExecutorTest : public testing::Test
{

};

void getExcept(int num)
{
	std::string str = "lalala";

	switch (num)
	{
	case 0:
		throw (gameexcept::bad_command(str));
		break;
	
	case 1:
		throw (gameexcept::bad_file(str));
		break;
	
	case 2:
		throw (gameexcept::out_of_range_on_field("X", 99));
		break;

	case 3:
		throw (gameexcept::out_of_range_on_step());
		break;
	}
}

TEST_F(ExecutorTest, Except)
{
	for (int i = 0; i < 3; ++i)
	{
		ASSERT_ANY_THROW(getExcept(i));
	}
}

TEST_F(ExecutorTest, ExecConstr)
{
	ASSERT_NO_THROW(controller::Executor temp);
}

TEST_F(ExecutorTest, ExecGetIter)
{
	controller::Executor exec;
	ASSERT_EQ(0, exec.getIteration());
}

TEST_F(ExecutorTest, ExecRunCorrect)
{
	controller::Executor exec;
	controller::Parser pars;
	model::Field fld;

	std::cout << "Write correct command here: ";
	EXPECT_NO_THROW(exec.run(fld, pars));
}

TEST_F(ExecutorTest, ExecRunIncorrect)
{
	controller::Executor exec;
	controller::Parser pars;
	model::Field fld;

	std::cout << "Write incorrect command here: ";
	EXPECT_ANY_THROW(exec.run(fld, pars));
}