#include <gtest/gtest.h>
#include "../Lab2/strategy.h"

class StrategyTest : public testing::Test
{

};

TEST_F(StrategyTest, recordConstr)
{
	ASSERT_NO_THROW(controller::record temp);
	ASSERT_NO_THROW(controller::record temp(0, 1, 2, 3));
}

TEST_F(StrategyTest, stratSet)
{
	model::Field fld;
	controller::Strategy strat;
	ASSERT_NO_THROW(strat.set(fld.getReferenceOnField(), 1, 1, true));
}

TEST_F(StrategyTest, stratStep)
{
	model::Field fld;
	controller::Strategy strat;
	ASSERT_NO_THROW(strat.step(fld, 100));
	ASSERT_NO_THROW(strat.step(fld, -100));
	ASSERT_NO_THROW(strat.step(fld, 100));
	ASSERT_NO_THROW(strat.step(fld, -111));
	ASSERT_NO_THROW(strat.step(fld, 0));
}

TEST_F(StrategyTest, stratIter)
{
	model::Field fld;
	controller::Strategy strat;

	strat.step(fld, 10);
	ASSERT_EQ(10, strat.getIteration());
	
	strat.step(fld, -50);
	ASSERT_EQ(0, strat.getIteration());
}

TEST_F(StrategyTest, stratIter2)
{
	model::Field fld;
	controller::Strategy strat;

	strat.set(fld.getReferenceOnField(), 5, 5, true);
	strat.set(fld.getReferenceOnField(), 5, 5, false);
	ASSERT_NO_THROW(strat.runCommandOnThisIter(fld.getReferenceOnField(), 0));
}

TEST_F(StrategyTest, stratReset)
{
	model::Field fld;
	controller::Strategy strat;

	strat.set(fld.getReferenceOnField(), 5, 4, true);
	strat.set(fld.getReferenceOnField(), 5, 5, true);
	strat.set(fld.getReferenceOnField(), 5, 6, true);

	strat.step(fld, 100);
	
	ASSERT_EQ(100, strat.getIteration());
	
	strat.reset(fld);
	
	ASSERT_EQ(0, strat.getIteration());
}