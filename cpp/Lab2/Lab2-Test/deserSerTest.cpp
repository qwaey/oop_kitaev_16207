#include <gtest/gtest.h>
#include "../Lab2/serializer.h"
#include "../Lab2/deserializer.h"

class DeserSerTest : public testing::Test
{

};

TEST_F(DeserSerTest, Ussd)
{
	ASSERT_EQ(controller::Serializer::getUssd(), controller::Deserializer::getUssd());
}

TEST_F(DeserSerTest, SaveLoad)
{
	model::Field world;
	model::Field world2;
	controller::Strategy strat;

	world.updateCurSize(20, 20);

	ASSERT_NO_THROW(controller::Serializer::save(world.getReferenceOnField(), "SaveTest.txt"));
	ASSERT_NO_THROW(controller::Deserializer::load(world2, strat, "SaveTest.txt"));

	ASSERT_EQ(world.getReferenceOnField()[0].size(), world2.getReferenceOnField()[0].size());
	ASSERT_EQ(world.getReferenceOnField().size(), world2.getReferenceOnField().size());

	world.updateCurSize(10, 10);

	ASSERT_NE(world.getReferenceOnField()[0].size(), world2.getReferenceOnField()[0].size());
	ASSERT_NE(world.getReferenceOnField().size(), world2.getReferenceOnField().size());
}