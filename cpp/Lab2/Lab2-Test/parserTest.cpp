#include <gtest/gtest.h>
#include "../Lab2/parser.h"

class ParserTest : public testing::Test
{

};

TEST_F(ParserTest, CommandNum)
{
	model::Field fld;
	controller::Parser pars;

	controller::Command com;
	com.pushArg("1");
	com.pushArg("2");

	ASSERT_EQ("2", com.getNextArg());
	ASSERT_EQ("1", com.getNextArg());

	com.pushArg("1");
	com.pushArg("2");

	ASSERT_NE(com.getNextArg(), com.getNextArg());
}

TEST_F(ParserTest, CommandArgs)
{
	controller::Command com;
	controller::Parser pars;

	com = pars.parsCommand("set 1 2");
	ASSERT_EQ(com.getNextArg(), "1");
	ASSERT_EQ(com.getNextArg(), "0");

	com = pars.parsCommand("clear 1 2");
	ASSERT_EQ(com.getNextArg(), "1");
	ASSERT_EQ(com.getNextArg(), "0");

	com = pars.parsCommand("step 1000");
	ASSERT_EQ(com.getNextArg(), "1000");
	
	com = pars.parsCommand("step");
	ASSERT_EQ(com.getNextArg(), "1");

	com = pars.parsCommand("step -100");
	ASSERT_EQ(com.getNextArg(), "-100");

	com = pars.parsCommand("back -100");
	ASSERT_EQ(com.getNextArg(), "-100");

	ASSERT_NO_THROW(pars.parsCommand("reset"));

	ASSERT_ANY_THROW(pars.parsCommand("set"));
	ASSERT_ANY_THROW(pars.parsCommand("clear"));
	ASSERT_ANY_THROW(pars.parsCommand("acsv"));
	ASSERT_ANY_THROW(pars.parsCommand(""));
}