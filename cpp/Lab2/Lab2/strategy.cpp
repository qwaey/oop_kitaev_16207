#include "strategy.h"

controller::record::record()
{
	iteration = 0;
	flag = false;
	x = 0;
	y = 0;
}


controller::record::record(size_t cIter, size_t cFlag, size_t cX, size_t cY)
{
	iteration = cIter;
	flag = cFlag;
	x = cX;
	y = cY;
}



size_t controller::Strategy::sumNeighbor(VVfield &field, size_t x, size_t y)
{
	size_t sum = 0;

	//viewing neighbors
	for (int i = -1; i <= 1; ++i)
	{
		for (int j = -1; j <= 1; ++j)
		{
			//misses herself
			if (i == 0 && j == 0)
			{
				continue;
			}

			if (field[moduleRing(field.size(), x + i)][moduleRing(field[0].size(), y + j)].condition())
			{
				++sum;
			}
		}
	}

	return sum;
}


size_t controller::Strategy::moduleRing(int module, int num)
{
	return ( (num < 0) ? (num + module) : (num >= module ? (num - module) : num ) );
}


void controller::Strategy::step(model::Field &world, int step)
{
	int curStep;
	bool fromZero = (step < 0);

	if (fromZero)
	{
		curStep = getIteration() + step; // calculate the next iteration
		partialHistoryCleaning(curStep); // clean unnecessary history (when returning)
		world.loadDefaultField(); // load the standard field
		clearIteration(); // reset the counter
	}
	else
	{
		curStep = step; // calculate the next iteration
	}

	editField(world.getReferenceOnField(), curStep, fromZero); // calculation of a new field condition
	return;
}


void controller::Strategy::editField(VVfield &field, int curStep, bool fromZero)
{
	// calculation by the number of iterations
	for (int i = 0; i < curStep; i++)
	{
		if (fromZero)
		{
			runCommandOnThisIter(field, i);
		}

		calcSumNeighbors(field);
		calcNextStep(field);
		incIteration();
	}
}


void controller::Strategy::partialHistoryCleaning(int curStep)
{
	// partial mopping up of history
	while (!historyList->empty() && historyList->back().iteration >= curStep)
	{
		historyList->pop_back();
	}
}


void controller::Strategy::calcSumNeighbors(VVfield &field)
{
	// counting the number of neighbors
	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			field[i][j].setNeighbor(sumNeighbor(field, i, j));
		}
	}
}


void controller::Strategy::calcNextStep(VVfield &field)
{
	// update field
	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			// 3 neighbor = new living cell
			if (!field[i][j].condition() && field[i][j].getNeighbor() == 3)
			{
				field[i][j].setCond(true);
			}

			// <2 neighbor = new dead cell
			if (field[i][j].condition() && field[i][j].getNeighbor() < 2)
			{
				field[i][j].setCond(false);
			}

			// >3 neighbor = new dead cell
			if (field[i][j].condition() && field[i][j].getNeighbor() > 3)
			{
				field[i][j].setCond(false);
			}
		}
	}
}


void controller::Strategy::reset(model::Field &world)
{
	// clear field
	for (int i = 0; i < world.getReferenceOnField().size(); i++)
	{
		for (int j = 0; j < world.getReferenceOnField()[0].size(); j++)
		{
			world.getReferenceOnField()[i][j].setCond(false);
		}
	}

	world.setDefaultField();
	clearHistory();
	clearIteration();
}

void controller::Strategy::correctLocation(std::string coordName, int coord, int size)
{
	if (coord < 0 || coord >= size)
	{
		throw (gameexcept::out_of_range_on_field(coordName, size));
	}
}

void controller::Strategy::set(VVfield &field, size_t x, size_t y, bool flag)
{
	correctLocation("x", x, field[0].size()); // check x
	correctLocation("y", y, field.size()); // check y

	// set condition and new event on history
	field[y][x].setCond(flag);
	historyList->push_back(controller::record(getIteration(), flag, x, y));
}

void controller::Strategy::runCommandOnThisIter(VVfield &field, int step)
{
	for (std::list< controller::record >::iterator Iter = historyList->begin(); Iter != historyList->end(); Iter++)
	{
		if (Iter->iteration > step)
		{
			return;
		}

		if (Iter->iteration == step)
		{
			set(field, Iter->x, Iter->y, Iter->flag);
			historyList->pop_back();
		}
	}
}

void controller::Strategy::clearHistory()
{
	historyList->clear();
}


size_t controller::Strategy::getIteration()
{
	return curIteration;
}


void controller::Strategy::incIteration()
{
	++curIteration;
}


void controller::Strategy::clearIteration()
{
	curIteration = 0;
}