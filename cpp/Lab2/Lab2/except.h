#ifndef EXCEPT_HEAD
#define EXCEPT_HEAD

#include "lib.h"

namespace gameexcept
{
	class except:std::exception
	{
	private:
		std::string message;

	public:
		except() {};
		virtual ~except() {};

		virtual const char * what() const throw()
		{
			return (message.c_str());
		}
	};


	class bad_command : public except
	{
	private:
		std::string message;

	public:
		bad_command(std::string &mes)
			: message("Unknown command: " + mes) {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};


	class bad_file : public except
	{
	private:
		std::string message;

	public:
		bad_file(std::string &mes)
			: message("Unable to open file with name: " + mes) {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};


	class out_of_range_on_field : public except
	{
	private:
		std::string message;

	public:
		out_of_range_on_field(std::string coord, int size)
			: message("The argument " + coord + " must be in the range [1; " + std::to_string(size) + "]") {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};


	class out_of_range_on_step : public except
	{
	private:
		std::string message;

	public:
		out_of_range_on_step()
			: message("The argument must be in the range [-99999; 999999]") {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};


	class end_of_game : public except
	{
	private:
		std::string message;

	public:
		end_of_game()
			: message("Thanks for the game!") {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};
}

#endif //EXCEPT_HEAD