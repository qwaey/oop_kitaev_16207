#include "cell.h"
#include "field.h"
#include "executor.h"


model::Field::Field()
{
	field.resize(height);
	defaultField.resize(height);

	for (int i = 0; i < height; i++)
	{
		field[i].resize(width);
		defaultField[i].resize(width);
	}
}


VVfield & model::Field::getReferenceOnField()
{
	return field;
}


void model::Field::updateCurSize(size_t w, size_t h)
{
	width = w;
	height = h;

	field.resize(height);
	defaultField.resize(height);

	for (int i = 0; i < height; i++)
	{
		field[i].resize(width);
		defaultField[i].resize(width);
	}
}


void model::Field::loadDefaultField()
{
	for (int i = 0; i < defaultField.size(); ++i)
	{
		for (int j = 0; j < defaultField[0].size(); ++j)
		{
			field[i][j].setCond(defaultField[i][j].condition());
		}
	}
}


void model::Field::setDefaultField()
{
	for (int i = 0; i < field.size(); ++i)
	{
		for (int j = 0; j < field[0].size(); ++j)
		{
			defaultField[i][j].setCond(field[i][j].condition());
		}
	}
}