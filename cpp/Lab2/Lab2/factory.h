#ifndef FACTORY_HEAD
#define FACTORY_HEAD

#include "lib.h"
#include "executor.h"
#include "parser.h"
#include "field.h"

class GenericObjectFactory
{
private:
	std::auto_ptr<controller::Executor> exec;
	std::auto_ptr<controller::Parser> pars;
	std::auto_ptr<model::Field> fld;

public:
	GenericObjectFactory();
	~GenericObjectFactory() {}

	controller::Executor *getExecutor() { return exec.get(); }
	controller::Parser *getParser() { return pars.get(); }
	model::Field *getField() { return fld.get(); }
};

#endif // FACTORY_HEAD