#include "command.h"

void controller::Command::setCommandNum(size_t commandNum)
{
	this->commandNum = commandNum;
}

size_t controller::Command::getCommandNum()
{
	return commandNum;
}

void controller::Command::pushArg(std::string str)
{
	argList.push_front(str);
}

std::string controller::Command::getNextArg()
{
	std::string temp = *(argList.begin());
	argList.pop_front();
	return temp;
}