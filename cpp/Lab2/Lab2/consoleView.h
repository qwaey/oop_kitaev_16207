#ifndef CONSOLE_VIEW_HEAD
#define CONSOLE_VIEW_HEAD

#include "lib.h"
#include "cell.h"
#include "field.h"

using VVfield = std::vector<std::vector<model::Cell>>;

namespace view
{
	const std::string textHelp = "The game supports the following commands:\n\
* reset - clear the field and the counter of moves\n\
* set X Y (where X is from 1 to (field width), Y is from 1 to (field height)) - set the body in a cell\n\
* clear X Y - clear the cell\n\
* step N - scroll the game forward by N steps (may be absent, then is assumed to be 1)\n\
* back N - scroll the game back by N steps (may be absent, then is assumed to be 1)\n\
* save ""filename"" - save the state of the game field to a text file in the current directory (no history of moves)\n\
* load ""filename"" - load the state of the game field from a text file in the current directory (with zeroing of the count of moves)\n\
* end - finishes the game;";

	const std::string textRules = "1. The game follows the steps\n\
2. The game is played on the toroidal field\n\
3. Each cell has 8 neighbors\n\
4. If an empty(dead) cell has exactly three neighbor - organism(living cells), then the next move in it gives birth to life(the body)\n\
5. If a living cell has less than two living neighbors, then it dies of being alone\n\
6. If a living cell has more than three living neighbors, then it dies from overpopulation\n\
7. The game ends if:\n\t* there are no living cells left on the field;\n\t* Live configuration does not change its stat";

	const std::string textEnd = "Game over";
	const std::string textSave = "The game was successfully saved!";

	class ConsoleView
	{
	public:
		static void help();
		static void rules();
		static void gameEnd();
		static void sucSave();
		static void printField(VVfield &, int); 
	};
}

#endif //CONSOLE_VIEW_HEAD