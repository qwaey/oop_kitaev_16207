#include "executor.h"

void controller::Executor::run(model::Field &world, Parser &parser)
{
		executeNextCommand(world, parser.readCommand(world));
}

void controller::Executor::executeNextCommand(model::Field &world, Command &newCommand)
{
	switch (newCommand.getCommandNum())
	{

	case RESET:
		strategy.reset(world);
		break;

	case SET:
		strategy.set(world.getReferenceOnField(), atoi(newCommand.getNextArg().c_str()), atoi(newCommand.getNextArg().c_str()), true);
		break;

	case CLEAR:
		strategy.set(world.getReferenceOnField(), atoi(newCommand.getNextArg().c_str()), atoi(newCommand.getNextArg().c_str()), false);
		break;

	case STEP:
		strategy.step(world, atoi(newCommand.getNextArg().c_str()));
		break;

	case BACK:
		strategy.step(world, -atoi(newCommand.getNextArg().c_str()));
		break;

	case SAVE:
		controller::Serializer::save(world.getReferenceOnField(), newCommand.getNextArg());
		view::ConsoleView::sucSave();
		break;

	case LOAD:
		controller::Deserializer::load(world, strategy, newCommand.getNextArg());
		break;

	case HELP:
		view::ConsoleView::rules();
		view::ConsoleView::help();
		break;

	case END:
		view::ConsoleView::gameEnd();
		throw (gameexcept::end_of_game());
		break;
	}
}

int controller::Executor::getIteration()
{
	return strategy.getIteration();
}
