#ifndef	GAME_HEAD
#define GAME_HEAD

#include "lib.h"
#include "factory.h"

using VVfield = std::vector<std::vector<model::Cell>>;

class Game
{
private:
	controller::Executor *Executor;
	model::Field *Field;
	controller::Parser *Parser;

public:
	Game(controller::Executor *, model::Field *, controller::Parser *);
	~Game() {}
	void run();
};

#endif //GAME_HEAD