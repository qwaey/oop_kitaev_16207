#include "parser.h"
#include "executor.h"
#include "field.h"
#include "command.h"

controller::Command controller::Parser::readCommand(model::Field &world)
{
	std::string str;
	getline(std::cin, str);
	return parsCommand(str);
}

controller::Command controller::Parser::parsCommand(std::string lineCommand)
{
	controller::Command nextCommand;

	std::istringstream commandStream(lineCommand);

	std::string newCommand;
	commandStream >> newCommand;

	nextCommand.setCommandNum(getCommandNum(newCommand));

	switch (nextCommand.getCommandNum())
	{

	case STEP:
		readStep(nextCommand, newCommand, commandStream);
		break;

	case BACK:
		readStep(nextCommand, newCommand, commandStream);
		break;

	case SET:
		readCoord(nextCommand, newCommand, commandStream);
		break;

	case CLEAR:
		readCoord(nextCommand, newCommand, commandStream);
		break;

	case LOAD:
		readFilename(nextCommand, newCommand, commandStream);
		break;

	case SAVE:
		readFilename(nextCommand, newCommand, commandStream);
		break;

	default:
		break;
	}

	return nextCommand;
}

void controller::Parser::readStep(Command &command, std::string newCommand, std::istringstream &commandStream)
{
	std::string num;

	if (!commandStream.eof())
	{
		commandStream >> num;
	}
	else
	{
		num = "1";
	}

	if (num.size() > 7 || !atoi(num.c_str()))
	{
		throw (gameexcept::out_of_range_on_step());
	}

	if (!commandStream.eof())
	{
		throw (gameexcept::bad_command(newCommand + " (Too many arguments to call)"));
	}

	command.pushArg(num);
}

void controller::Parser::readCoord(Command &command, std::string newCommand, std::istringstream &commandStream)
{
	size_t num = 0;

	for (int i = 0; i < 2; i++)
	{
		if (!commandStream.eof())
		{
			commandStream >> num;
		}
		else
		{
			throw (gameexcept::bad_command(newCommand + " (There are not enough arguments to call)"));
		}

		command.pushArg(std::to_string(num - 1));
	}

	if (!commandStream.eof())
	{
		throw (gameexcept::bad_command(newCommand + " (Too many arguments to call)"));
	}
}

void controller::Parser::readFilename(Command &command, std::string newCommand, std::istringstream &commandStream)
{
	std::string filename;

	if (!commandStream.eof())
	{
		commandStream >> filename;
	}
	else
	{
		throw (gameexcept::bad_command(newCommand + " (Called without arguments)"));
	}

	command.pushArg(filename);
}

size_t controller::Parser::getCommandNum(std::string command)
{
	std::string arr[9] = { "reset", "set", "clear", "step", "back", "save", "load", "help", "end" };

	for (size_t i = 0; i < 9; i++)
	{
		if (command == arr[i])
		{
			return i;
		}
	}

	throw (gameexcept::bad_command(command));
}