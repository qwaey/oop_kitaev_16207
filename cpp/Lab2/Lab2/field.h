#ifndef FIELD_HEAD
#define FIELD_HEAD

#include "lib.h"
#include "cell.h"

using VVfield = std::vector<std::vector<model::Cell>>;

namespace model
{
	class Field
	{
	private:
		VVfield field;
		
		// can be modified when loading from a file
		size_t width = 10; 
		size_t height = 10;
		VVfield defaultField;

	public:
		Field();
		~Field() {};

		VVfield &getReferenceOnField();
		void updateCurSize(size_t, size_t);
		void loadDefaultField();
		void setDefaultField();
	};
}

#endif //FIELD_HEAD