#ifndef	EXECUTOR_HEAD
#define EXECUTOR_HEAD

#include "lib.h"
#include "field.h"
#include "parser.h"
#include "consoleView.h"
#include "strategy.h"
#include "serializer.h"
#include "deserializer.h"
#include "command.h"

using VVfield = std::vector<std::vector<model::Cell>>;
enum commandList { RESET, SET, CLEAR, STEP, BACK, SAVE, LOAD, HELP, END };

namespace controller
{
	class Executor
	{
	private:
		Strategy strategy;
		void executeNextCommand(model::Field &, Command &);

	public:
		void run(model::Field &, Parser &);
		int getIteration();
	};
}

#endif //EXECUTOR_HEAD