#ifndef COMMAND_HEAD
#define COMMAND_HEAD

#include "lib.h"

namespace controller
{
	class Command
	{
	private:
		std::list< std::string > argList;
		size_t commandNum;

	public:
		Command() {};

		size_t getCommandNum();
		void setCommandNum(size_t);
		void pushArg(std::string);
		std::string getNextArg();
	};
}

#endif // COMMAND_HEAD