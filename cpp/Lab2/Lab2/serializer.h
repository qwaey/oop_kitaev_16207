#ifndef SERIALIZER_HEAD
#define SERIALIZER_HEAD

#include "lib.h"
#include "cell.h"
#include "field.h"
#include "except.h"

namespace controller
{
	class Serializer
	{
	private:
		static const int ussd = 777;

	public:
		static int getUssd();
		static void save(VVfield &, std::string);
		static void writeProperties(VVfield &, std::ofstream &);
		static void writeField(VVfield &, std::ofstream &);
	};
}

#endif //SERIALIZER_HEAD