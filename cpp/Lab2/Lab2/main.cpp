#include "game.h"

int main()
{
	GenericObjectFactory classFactory;
	Game game(classFactory.getExecutor(), classFactory.getField(), classFactory.getParser());
	game.run();
	return 0;
}