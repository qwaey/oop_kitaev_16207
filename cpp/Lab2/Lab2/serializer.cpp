#include "serializer.h"


int controller::Serializer::getUssd()
{
	return ussd;
}


void controller::Serializer::save(VVfield &field, std::string filename)
{
	std::ofstream fout(filename);

	if (!fout.is_open())
	{
		throw (gameexcept::bad_file(filename));
	}

	writeProperties(field, fout);
	writeField(field, fout);
	fout.close();
}


void controller::Serializer::writeProperties(VVfield &field, std::ofstream &fout)
{
	// ussd, width, height
	fout << ussd << ' ' << field.size() << ' ' << field[0].size() << std::endl;
}


void controller::Serializer::writeField(VVfield &field, std::ofstream &fout)
{
	// write field on file
	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			fout << field[i][j].condition();
		}
	}
}