#include "cell.h"

model::Cell::Cell(bool b)
{
	setCond(b);
}

bool model::Cell::condition()
{
	return cond;
}

char model::Cell::getCharCond()
{
	return (cond ? 'o' : '.');
}

void model::Cell::setCond(bool newCond)
{
	cond = newCond;
}

void model::Cell::setCond(char sim)
{
	cond = (sim == 'o' || sim == '1');
}

void model::Cell::setNeighbor(size_t num)
{
	numNeighbor = num;
}

size_t model::Cell::getNeighbor()
{
	return numNeighbor;
}