#include "deserializer.h"


int controller::Deserializer::getUssd()
{
	return ussd;
}


void controller::Deserializer::load(model::Field &world, controller::Strategy &strategy, std::string filename)
{
	std::ifstream fin(filename);

	if (!fin.is_open())
	{
		throw (gameexcept::bad_file(filename));
	}

	if (!checkCorrectFile(fin))
	{
		throw (gameexcept::bad_file(filename));
	}

	strategy.clearHistory();

	fieldPreparation(world, strategy, fin);
	fieldFilling(world, fin);
	fin.close();
}


bool controller::Deserializer::checkCorrectFile(std::ifstream &fin)
{
	int ussdCheck;
	fin >> ussdCheck;
	return (getUssd() == ussdCheck);
}


void controller::Deserializer::fieldPreparation(model::Field &world, controller::Strategy &strat, std::ifstream &fin)
{
	// read new size
	size_t newW, newH;
	fin >> newW >> newH;

	// clear old informartion on field
	strat.reset(world);

	// set new width and height
	world.updateCurSize(newW, newH);
}


void controller::Deserializer::fieldFilling(model::Field &world, std::ifstream &fin)
{
	// field filling
	for (size_t i = 0; i < world.getReferenceOnField().size(); i++)
	{
		for (size_t j = 0; j < world.getReferenceOnField()[0].size(); j++)
		{
			char sim;
			fin >> sim;
			world.getReferenceOnField()[i][j].setCond(sim);
		}
	}

	world.setDefaultField();
}
