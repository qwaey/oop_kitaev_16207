#include "factory.h"

GenericObjectFactory::GenericObjectFactory()
	: exec(new controller::Executor)
	, fld(new model::Field)
	, pars(new controller::Parser)
{
}