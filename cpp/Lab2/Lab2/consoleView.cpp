#include "consoleView.h"

void view::ConsoleView::help()
{
	std::cout << view::textHelp << std::endl;
	system("pause");
}

void view::ConsoleView::rules()
{
	system("cls");
	std::cout << textRules << std::endl;
	system("pause");
}

void view::ConsoleView::gameEnd()
{
	std::cout << textEnd << std::endl;
}

void view::ConsoleView::sucSave()
{
	std::cout << textSave << std::endl;
	system("pause");
}

void view::ConsoleView::printField(VVfield &field, int iteration)
{
	std::cout << "Iteration: " << iteration << std::endl;

	for (int i = 0; i <= field[0].size(); i++)
	{
		std::cout << std::setw(3) << i;
	}

	std::cout << std::endl;

	for (int i = 0; i < field.size(); i++)
	{
		std::cout << std::setw(3) << (i + 1);

 		for (int j = 0; j < field[0].size(); j++)
		{
			std::cout << std::setw(3) << (field[i][j]).getCharCond();
		}
		
		std::cout << std::endl;
	}

	std::cout << "Enter the command here: ";
}