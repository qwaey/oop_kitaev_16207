#include "game.h"

Game::Game(controller::Executor *exec, model::Field *fld, controller::Parser *pars)
	: Executor(exec)
	, Field(fld)
	, Parser(pars)
{
}


void Game::run()
{
	view::ConsoleView::rules();

	while (true)
	{
		try
		{
			system("cls");
			view::ConsoleView::printField(Field->getReferenceOnField(), Executor->getIteration());
			Executor->run(*Field, *Parser);
		}
		catch (gameexcept::end_of_game &mes)
		{
			std::cout << mes.what() << std::endl;
			return;
		}
		catch (gameexcept::except &mes)
		{
			system("cls");
			std::cout << mes.what() << "\n---------------" << std::endl;
			view::ConsoleView::help();
		}
		catch (std::exception &mes)
		{
			std::cout << mes.what() << std::endl;
			view::ConsoleView::help();
			return;
		}
	}
}