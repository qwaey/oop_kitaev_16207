#ifndef DESERIALIZER_HEAD
#define DESERIALIZER_HEAD

#include "lib.h"
#include "cell.h"
#include "field.h"
#include "strategy.h"
#include "except.h"

namespace controller
{
	class Deserializer
	{
	private:
		static const int ussd = 777;

	public:
		static int getUssd();
		static void load(model::Field &, controller::Strategy &, std::string);
		static bool checkCorrectFile(std::ifstream &);
		static void fieldPreparation(model::Field &, controller::Strategy &, std::ifstream &);
		static void fieldFilling(model::Field &, std::ifstream &);
	};
}

#endif //DESERIALIZER_HEAD