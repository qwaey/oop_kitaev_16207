#ifndef CELL_HEAD
#define CELL_HEAD

#include "lib.h"

namespace model
{
	class Cell
	{
	private:
		bool cond = false;
		size_t numNeighbor = 0;

	public:
		Cell() {};
		Cell(bool);

		bool condition();
		char getCharCond();
		void setCond(bool);
		void setCond(char);
		void setNeighbor(size_t);
		size_t getNeighbor();
	};
}

#endif //CELL_HEAD