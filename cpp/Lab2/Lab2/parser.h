#ifndef PARSER_HEAD
#define PARSER_HEAD

#include "lib.h"
#include "field.h"
#include "command.h"
#include "except.h"

using VVfield = std::vector<std::vector<model::Cell>>;

namespace controller
{
	class Parser
	{
	private:
		size_t getCommandNum(std::string);

	public:
		Command parsCommand(std::string);
		Command readCommand(model::Field &);
		void readStep(Command &, std::string, std::istringstream &) ;
		void readCoord(Command &, std::string, std::istringstream &);
		void readFilename(Command &, std::string, std::istringstream &);
	};
}

#endif //PARSER_HEAD