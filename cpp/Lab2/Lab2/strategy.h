#ifndef STRATEGY_HEAD
#define STRATEGY_HEAD

#include "lib.h"
#include "cell.h"
#include "field.h"
#include "parser.h"

namespace controller
{
	class record
	{
	public:
		record();
		record(size_t, size_t, size_t, size_t);

		size_t iteration;
		size_t flag;
		size_t x;
		size_t y;
	};

	class Strategy
	{
	private:
		size_t sumNeighbor(VVfield &, size_t, size_t);
		void calcNextStep(VVfield &);
		void calcSumNeighbors(VVfield &);
		void partialHistoryCleaning(int);
		void editField(VVfield &, int, bool);
		size_t moduleRing(int, int);
		size_t curIteration = 0;

		std::auto_ptr<std::list< record >> historyList;

	public:
		Strategy()
			: historyList(new std::list< record >) {}
		~Strategy() {}

		void step(model::Field &, int);
		void reset(model::Field &);
		void correctLocation(std::string, int, int);
		void set(VVfield &, size_t, size_t, bool);

		void runCommandOnThisIter(VVfield &, int);
		void clearHistory();

		size_t getIteration();
		void incIteration();
		void clearIteration();
	};
}

#endif //STRATEGY_HEAD