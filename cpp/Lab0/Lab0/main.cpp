#include "module1.h"
#include "module2.h"
#include "module3.h"
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	cout << "Hello world!" << "\n";

	cout << Module1::getMyName() << "\n";
	cout << Module2::getMyName() << "\n";

	using namespace Module1;
	cout << getMyName() << "\n"; // (A) John
	cout << Module2::getMyName() << "\n";

	cout << Module2::getMyName() << "\n"; // COMPILATION ERROR (C)

	cout << Module2::getMyName() << "\n"; // (D) James

	cout << Module3::getMyName() << "\n"; // Peter

	return 0;
}