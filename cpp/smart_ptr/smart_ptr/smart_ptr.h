#pragma once

template< class T >
class AutoPtr
{
private:
	T *ptr;

public:

	AutoPtr(T *ap)
		: ptr(ap) {}

	~AutoPtr()
	{
		delete ptr;
	}

	AutoPtr(AutoPtr &ap)
		: ptr(ap)
	{
		ap = nullptr;
	}

	AutoPtr &operator=(AutoPtr &ap)
	{
		if (this == &ap)
		{
			return (*this);
		}

		delete this->ptr;
		this->ptr = ap.ptr;
		ap.ptr = nullptr;
	}

	T *operator->()
	{
		return this->ptr;
	}

	T operator*()
	{
		return *ptr;
	}

	void reset(T *ptr)
	{
		delete ptr;
	}

	T *get()
	{
		return *ptr;
	}
};