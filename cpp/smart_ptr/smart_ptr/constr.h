#pragma once

#include <iostream>

class C1
{
public:
	C1()
	{
		std::cout << "C1()\n";
	}

	~C1()
	{
		std::cout << "~C1()\n";
	}
};

class C2
{
public:
	C2()
	{
		std::cout << "C2()\n";
	}

	~C2()
	{
		std::cout << "~C2()\n";
	}
};

class A
{
public:
	C1 c1;

	A()
	{
		std::cout << "A()\n";
	}

	~A()
	{
		std::cout << "~A()\n";
	}
};

class B : public A
{
public:
	C2 c2;

	B()
	{
		std::cout << "B()\n";
	}

	~B()
	{
		std::cout << "~B()\n";
	}
};