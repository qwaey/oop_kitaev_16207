#pragma once

class Singleton
{
private:
	static Singleton *single;
	bool state = false;

	Singleton() {}
	Singleton(Singleton &) {}
	Singleton &operator=(Singleton &) {}

public:
	static Singleton *getSingleton()
	{
		if (!single)
		{
			single = new Singleton();
		}

		return single;
	}

	bool get_state()
	{
		return state;
	}
};