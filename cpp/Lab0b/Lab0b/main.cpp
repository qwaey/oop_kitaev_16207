#include "head.h"

int main(int argc, char *argv[])
{	
	string IN_FILE_ARG_INDEX = argv[1];
	string OUT_FILE_ARG_INDEX = argv[2];
	list <string> buf;
	string temp;
	
	if (argc < 3)
	{
		cout << "Usage: Lab0b.exe <input_file> <output_file>" << endl;
		return EXIT_FAILURE;
	}

	ifstream fin(IN_FILE_ARG_INDEX);
	ofstream fout(OUT_FILE_ARG_INDEX);

	if (!fin)
	{
		cout << "error opening input file" << endl;
		return EXIT_FAILURE;
	}

	if (!fout)
	{
		cout << "error opening output file" << endl;
		return EXIT_FAILURE;
	}

	while (getline(fin, temp))
		buf.push_back(temp);

	Sort::sort_string(buf);

	while (!(buf.empty()))
	{
		fout << buf.front() << endl;
		buf.pop_front();
	}

	fin.close();
	fout.close();

	return EXIT_SUCCESS;
}