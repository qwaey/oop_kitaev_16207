#include <gtest/gtest.h>

#include "../Lab3/dump.h"
#include "../Lab3/grep.h"
#include "../Lab3/readFile.h"
#include "../Lab3/replace.h"
#include "../Lab3/sort.h"
#include "../Lab3/writeFile.h"

class CommandTest : public testing::Test
{

};

using WorkList = std::list< std::string >;

TEST_F(CommandTest, readfile_and_writefile)
{	
	std::string FILENAME = "readfileTest.txt";
	
	WorkList WL;
	WL.push_back("Test");
	WL.push_back("Read");
	WL.push_back("and");
	WL.push_back("Write");
	
	// WRITEFILE TEST
	command::WriteFile writeFile(FILENAME);
	writeFile.process(&WL);

	ASSERT_TRUE(WL.empty());

	// READFILE TEST
	command::ReadFile readFile(FILENAME);	
	readFile.process(&WL);

	WorkList WL2;
	WL2.push_back("Test");
	WL2.push_back("Read");
	WL2.push_back("and");
	WL2.push_back("Write");

	for (auto Iter1 = WL.begin(), Iter2 = WL2.begin(); Iter2 != WL2.end(); Iter1++, Iter2++)
	{
		ASSERT_EQ(*Iter1, *Iter2);
	}
}

TEST_F(CommandTest, sortTest)
{
	WorkList WL;
	WL.push_back("Test");
	WL.push_back("Read");
	WL.push_back("and");
	WL.push_back("Write");
	
	command::Sort sort;
	sort.process(&WL);
	
	WorkList SORT;
	SORT.push_back("Read");
	SORT.push_back("Test");
	SORT.push_back("Write");
	SORT.push_back("and");

	for (auto Iter1 = WL.begin(), Iter2 = SORT.begin(); Iter1 != WL.end(); Iter1++, Iter2++)
	{
		ASSERT_EQ(*Iter1, *Iter2);
	}
}

TEST_F(CommandTest, grepTest)
{
	WorkList WL;
	WL.push_back("Test");
	WL.push_back("Read");
	WL.push_back("and");
	WL.push_back("Write");

	command::Grep grep("and");
	grep.process(&WL);

	WorkList GREP;
	GREP.push_back("and");

	for (auto Iter1 = WL.begin(), Iter2 = GREP.begin(); Iter1 != WL.end(); Iter1++, Iter2++)
	{
		ASSERT_EQ(*Iter1, *Iter2);
	}
}

TEST_F(CommandTest, replaceTest)
{
	WorkList WL;
	WL.push_back("Test");
	WL.push_back("Read");
	WL.push_back("and");
	WL.push_back("Write");

	command::Replace replace("and", "or");
	replace.process(&WL);

	WorkList REPLACE;
	REPLACE.push_back("Test");
	REPLACE.push_back("Read");
	REPLACE.push_back("or");
	REPLACE.push_back("Write");

	for (auto Iter1 = WL.begin(), Iter2 = REPLACE.begin(); Iter1 != WL.end(); Iter1++, Iter2++)
	{
		ASSERT_EQ(*Iter1, *Iter2);
	}
}

TEST_F(CommandTest, dumpTest)
{
	std::string FILENAME = "readfileTest.txt";

	WorkList WL;
	WL.push_back("Test");
	WL.push_back("Read");
	WL.push_back("and");
	WL.push_back("Write");

	command::Dump dump(FILENAME);
	dump.process(&WL);

	command::ReadFile readFile(FILENAME);
	readFile.process(&WL);

	WorkList WL2;
	WL2.push_back("Test");
	WL2.push_back("Read");
	WL2.push_back("and");
	WL2.push_back("Write");

	for (auto Iter1 = WL.begin(), Iter2 = WL2.begin(); Iter2 != WL2.end(); Iter1++, Iter2++)
	{
		ASSERT_EQ(*Iter1, *Iter2);
	}
}