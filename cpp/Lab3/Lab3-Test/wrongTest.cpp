#include <gtest/gtest.h>

#include "../Lab3/lib.h"
#include "../Lab3/factory.h"
#include "../Lab3/executor.h"

class WrongTest : public testing::Test
{

};

using WorkList = std::list< std::string >;

TEST_F(WrongTest, wrong1)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_ANY_THROW(executor.run("wrong_in_1.txt"));
}

TEST_F(WrongTest, wrong2)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_ANY_THROW(executor.run("wrong_in_2.txt"));
}

TEST_F(WrongTest, wrong3)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_ANY_THROW(executor.run("wrong_in_3.txt"));
}

TEST_F(WrongTest, wrong4)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_ANY_THROW(executor.run("wrong_in_4.txt"));
}

TEST_F(WrongTest, wrong5)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_ANY_THROW(executor.run("wrong_in_5.txt"));
}