#include <gtest/gtest.h>

#include "../Lab3/lib.h"
#include "../Lab3/factory.h"
#include "../Lab3/executor.h"

class CorrectTest : public testing::Test
{

};

using WorkList = std::list< std::string >;

TEST_F(CorrectTest, correct1)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_NO_THROW(executor.run("correct_in_1.txt"));
}

TEST_F(CorrectTest, correct2)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_NO_THROW(executor.run("correct_in_2.txt"));
}

TEST_F(CorrectTest, correct3)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_NO_THROW(executor.run("correct_in_3.txt"));
}

TEST_F(CorrectTest, correct4)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_NO_THROW(executor.run("correct_in_4.txt"));
}

TEST_F(CorrectTest, correct5)
{
	Parser parser;
	Executor executor(parser);
	ASSERT_NO_THROW(executor.run("correct_in_5.txt"));
}