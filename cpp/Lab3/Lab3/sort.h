#ifndef SORT_HEAD
#define SORT_HEAD

#include "lib.h"
#include "worker.h"

namespace command
{
	class Sort : public Worker
	{
	private:
		std::string comName = "sort";

	public:
		void process(WorkList *) override;
	};
}

#endif // SORT_HEAD