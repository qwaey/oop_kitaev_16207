#include "parser.h"


Parser::Parser()
{
	ListConveyor = new WorkerList;
	argMap = WorkFactory::getArgMap();
}

Parser::~Parser()
{
	delete ListConveyor;
}

void Parser::pars(std::string filename)
{
	std::ifstream fin(filename);

	if (!fin.is_open())
	{
		throw(std::ifstream::failure(filename + " there is no way to open the file"));
	}

	try
	{
		fileParsing(filename, fin);

		/*
		check that "readfile" is the first command, and the options after "writefile";
		And "writefile" is the last command.
		*/
		checkSequence(filename);
	}
	catch (std::exception &mes)
	{
		fin.close();
		throw;
	}

	fin.close();
}

void Parser::fileParsing(std::string filename, std::ifstream &fin)
{
	if (fin.eof())
	{
		throw(std::ifstream::failure("line: " + std::to_string(NumOfLine) + "; " + filename + "; unexpected end of file"));
	}

	std::string label;
	parsline(fin, label);

	if (label == "desc")
	{
		parsDesc(filename, fin);
	}
	else if (label == "csed")
	{
		parsCsed(filename, fin);
	}
	else
	{
		throw(std::ifstream::failure("line: " + std::to_string(NumOfLine) + "; " + filename + ": unidentified label"));
	}
}

void Parser::parsDesc(std::string filename, std::ifstream &fin)
{
	while (!fin.eof())
	{
		std::string str;
		parsline(fin, str);

		if (str == "csed")
		{
			parsCsed(filename, fin);
			return;
		}

		std::istringstream lineStream(str);
		checkBlock(filename, lineStream, getCommandId(filename, lineStream));
	}
}

void Parser::parsCsed(std::string filename, std::ifstream &fin)
{
	while (!fin.eof())
	{
		std::string str;
		parsline(fin, str);

		if (str == "desc")
		{
			parsDesc(filename, fin);
			return;
		}

		std::istringstream lineStream(str);
		checkConveyor(filename, lineStream);
	}
}





void Parser::parsNextPiece(std::string filename, std::istringstream &fin, std::string &str)
{
	if (!fin.eof())
	{
		fin >> str;
	}
	else
	{
		throw(std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; " + filename + "; unexpected end of file"));
	}
}



int Parser::getIntId(std::string str)
{
	int id = atoi(str.c_str());

	if (id < 0)
	{
		throw (std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; id must be >= 0"));
	}

	return id;
}



Parser::WorkerList * Parser::getWorkerList()
{
	for (auto Iter = ListConveyor->begin(); Iter != ListConveyor->end(); Iter++)
	{
		try
		{
			Iter->second = WorkFactory::getDecision(&idMap, Iter->first);
		}
		catch (std::out_of_range &mes)
		{
			std::cerr << mes.what() << std::endl;
			delete ListConveyor;
			throw;
		}
	}

	return ListConveyor;
}



/// 1-st arg: MyString; 2-nd arg: separating character;
void Parser::checkSepChar(std::string myStr, std::string SepChar)  
{
	if (myStr != SepChar)
	{
		throw (std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; after id should follow the operator \"" + SepChar + "\""));
	}
}




int Parser::getCommandId(std::string filename, std::istringstream &fin)
{
	std::string str;

	parsNextPiece(filename, fin, str);

	int id = getIntId(str);

	if (idMap.find(id) != idMap.end())
	{
		throw (std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; repeating identifier"));
	}
	
	parsNextPiece(filename, fin, str);
	checkSepChar(str, "=");

	return id;
}




void Parser::checkBlock(std::string filename, std::istringstream &fin, int id)
{
	std::string str;
	parsNextPiece(filename, fin, str);
	
	// we check the correctness of the command and consider the number of arguments
	int arg = getNumOfArgsCommand(str);

	// insert the command by its identifier
	idMap[id] = make_pair(str, new std::list<std::string>);

	// insert the arguments of the command
	for (int i = 0; i < arg; i++)
	{
		parsNextPiece(filename, fin, str);
		idMap.at(id).second->push_back(str);
	}

	if (!fin.eof())
	{
		throw (std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; only " + std::to_string(argMap.at(idMap.at(id).first)) + " arguments were expected"));
	}
}



int Parser::getNumOfArgsCommand(std::string str)
{
	return argMap.at(str);
}




void Parser::checkConveyor(std::string filename, std::istringstream & fin)
{
	std::string str;
	
	while (!fin.eof())
	{
		parsNextPiece(filename, fin, str); // read command id
		ListConveyor->push_back(std::make_pair(getIntId(str), nullptr)); // and push id in list

		if (fin.eof())
		{
			break;
		}
		
		parsNextPiece(filename, fin, str);
		checkSepChar(str, "->");

		if (fin.eof())
		{
			throw(std::istringstream::failure("line: " + std::to_string(NumOfLine) + "; " + filename + "; unexpected end of file"));
		}
	}
}

void Parser::checkSequence(std::string filename)
{
	std::map<std::string, commandList> comMap = WorkFactory::getComMap();
	bool Automat = false;

	for (auto Iter = ListConveyor->begin(); Iter != ListConveyor->end(); Iter++)
	{
		switch (comMap.at(idMap.at(Iter->first).first))
		{

		case READFILE:
			if (!Automat)
			{
				Automat = true;
			}
			else
			{
				throw(workexcept::conveyor_error(filename + ": readfile must be only first command of the conveyor or go after the command \"writefile\""));
			}
			break;

		case WRITEFILE:
			if (Automat)
			{
				Automat = false;
			}
			else
			{
				throw(workexcept::conveyor_error(filename + ": writefile can not be the first conveyor command or go after the command \"writefile\""));
			}
			break;

		default:
			if (!Automat)
			{
				throw(workexcept::conveyor_error(filename + ": " + idMap.at(Iter->first).first + " can not be the first conveyor command or go after the command \"writefile\""));
			}
			break;

		}
	}

	if (Automat)
	{
		throw(workexcept::conveyor_error(filename + ": text was not written down after processing\nThe conveyor should end with the command \"writefile <filename>\""));
	}
}

void Parser::parsline(std::ifstream &fin, std::string &str)
{
	getline(fin, str);
	NumOfLine++;
}
