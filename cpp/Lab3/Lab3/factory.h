#ifndef FACTORY_HEAD
#define FACTORY_HEAD

#include "parser.h"
#include "executor.h"

class Factory
{
public:
	static Parser * getParser();
	static Executor * getExecutor(BaseParser *);
};


#endif // FACTORY_HEAD