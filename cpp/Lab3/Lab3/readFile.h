#ifndef READFILE_HEAD
#define READFILE_HEAD

#include "worker.h"

namespace command
{
	class ReadFile : public Worker
	{
	private:
		std::string filename;

	public:
		ReadFile(std::string name) { filename = name; }

		void process(WorkList *) override;
	};
}

#endif // READFILE_HEAD