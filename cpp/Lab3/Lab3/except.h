#ifndef EXCEPT_HEAD
#define EXCEPT_HEAD

#include "lib.h"

namespace workexcept
{
	class except : public std::exception
	{
	private:
		std::string message;

	public:
		except() {};
		virtual ~except() {};

		virtual const char * what() const throw()
		{
			return (message.c_str());
		}
	};


	class conveyor_error : public except
	{
	private:
		std::string message;

	public:
		conveyor_error(std::string &mes)
			: message("Conveyor error: " + mes) {}

		const char *what() const throw() override
		{
			return (message.c_str());
		}
	};
}

#endif //EXCEPT_HEAD