#include "workFactory.h"

const WorkFactory::CommandMap WorkFactory::getComMap()
{
	static const CommandMap commandMap = { { "readfile" , READFILE },
									{ "writefile" , WRITEFILE },
									{ "grep", GREP },
									{ "sort", SORT },
									{ "replace", REPLACE },
									{ "dump", DUMP } };
	return commandMap;
}

const WorkFactory::ArgMap WorkFactory::getArgMap()
{
	static const ArgMap argMap = { { "readfile" , 1 },
							{ "writefile" , 1 },
							{ "grep", 1 },
							{ "sort", 0 },
							{ "replace", 2 },
							{ "dump", 1 } };
	return argMap;
}

Worker * WorkFactory::getDecision(IdMap *idMap, int id)
{
	int i = 0;
	CommandMap comMap = getComMap();
	std::vector<std::string> *argVect = new std::vector<std::string>(idMap->at(id).second->size());
	Worker *worker;

	// fill the argument vector
	for (auto Iter = idMap->at(id).second->begin(); Iter != idMap->at(id).second->end(); Iter++, i++)
	{
		(*argVect)[i] = (*Iter);
	}

	switch (comMap.at(idMap->at(id).first))
	{

	case READFILE:
		worker = new command::ReadFile((*argVect)[0]);
		break;
	
	case WRITEFILE:
		worker = new command::WriteFile((*argVect)[0]);
		break;

	case GREP:
		worker = new command::Grep((*argVect)[0]);
		break;

	case SORT:
		worker = new command::Sort();
		break;

	case REPLACE:
		worker = new command::Replace((*argVect)[0], (*argVect)[1]);
		break;

	case DUMP:
		worker = new command::Dump((*argVect)[0]);
		break;

	default:
		throw(std::out_of_range ("Indefinite id: " + std::to_string(id)));
		break;
	}

	delete argVect;
	return worker;
}