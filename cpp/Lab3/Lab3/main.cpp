#include "lib.h"
#include "factory.h"

int main(int argc, char *argv[])
{
	const std::string FILENAME = argv[1];

	BaseExecutor *executor = Factory::getExecutor(Factory::getParser());

	try
	{
		executor->run(FILENAME);
	}
	catch (std::exception e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}