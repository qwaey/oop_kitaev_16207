#include "readFile.h"

void command::ReadFile::process(WorkList *list)
{
	std::ifstream fin(filename);

	if (!fin.is_open())
	{
		throw(std::ifstream::failure(filename + " there is no way to open the file"));
	}

	while (!fin.eof())
	{
		std::string str;
		std::getline(fin, str);
		list->push_back(str);
	}
}