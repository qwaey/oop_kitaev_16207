#ifndef WORKER_HEAD
#define WORKER_HEAD

#include "lib.h"
#include "except.h"

class Worker
{
public:
	using WorkList = std::list< std::string >;

	virtual void process(WorkList *) = 0;
};

#endif //WORKER_HEAD