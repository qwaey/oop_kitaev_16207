#include "writeFile.h"

void command::WriteFile::process(WorkList *list)
{
	std::ofstream fout(filename);

	if (!fout.is_open())
	{
		throw(std::ofstream::failure(filename + " there is no way to open the file"));
	}

	auto Iter = list->begin();
	fout << *Iter;

	for (Iter++ ; Iter != list->end(); Iter++)
	{
		fout << "\n" << *Iter;
	}

	list->clear();
	fout.close();
}