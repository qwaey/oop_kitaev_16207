#ifndef REPLACE_HEAD
#define REPLACE_HEAD

#include "worker.h"

namespace command
{
	class Replace : public Worker
	{
	private:
		int counter = 0;
		std::string word[2];

	public:
		Replace(std::string, std::string);

		void process(WorkList *) override;
	};
}

#endif // REPLACE_HEAD