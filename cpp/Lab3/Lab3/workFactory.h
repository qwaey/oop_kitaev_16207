#ifndef WORKFACTORY_HEAD
#define WORKFACTORY_HEAD

#include "dump.h"
#include "grep.h"
#include "readFile.h"
#include "replace.h"
#include "sort.h"
#include "writeFile.h"

static enum commandList { READFILE, WRITEFILE, GREP, SORT, REPLACE, DUMP };

class WorkFactory
{
private:
	using IdMap = std::map<int, std::pair<std::string, std::list<std::string> *>>;
	using CommandMap = std::map<std::string, commandList>;
	using ArgMap = std::map<std::string, int>;

public:
	static const CommandMap getComMap();
	static const ArgMap getArgMap();
	static Worker * getDecision(IdMap *, int);
};

#endif // WORKFACTORY_HEAD