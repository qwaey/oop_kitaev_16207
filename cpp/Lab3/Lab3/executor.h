#ifndef EXECUTOR_HEAD
#define EXECUTOR_HEAD

#include "lib.h"
#include "baseClass.h"

class Executor : public BaseExecutor
{
private:
	BaseParser &parser;

	using WorkerList = std::list<std::pair<int, Worker *>>;
	WorkerList *workerList;

	using WorkList = std::list< std::string >;

public:
	Executor(BaseParser &);

	void run(std::string) override;
};

#endif // EXECUTOR_HEAD