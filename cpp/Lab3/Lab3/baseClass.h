#ifndef BASECLASS_HEAD
#define BASECLASS_HEAD

#include "worker.h"

class BaseExecutor
{
public:
	virtual void run(std::string) = 0;
};

class BaseParser
{
public:
	virtual void pars(std::string) = 0;
	virtual std::list<std::pair<int, Worker *>> *getWorkerList() = 0;
};

#endif // BASECLASS_HEAD