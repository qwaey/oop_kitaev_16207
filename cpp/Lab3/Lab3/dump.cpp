#include "dump.h"

void command::Dump::process(WorkList *list)
{
	std::ofstream fout(filename);

	if (!fout.is_open())
	{
		throw(std::ofstream::failure(filename + " there is no way to open the file"));
	}

	for (auto Iter = list->begin(); Iter != list->end(); Iter++)
	{
		fout << *Iter << "\n";
	}

	fout.close();
}