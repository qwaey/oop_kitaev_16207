#ifndef DUMP_HEAD
#define DUMP_HEAD

#include "worker.h"

namespace command
{
	class Dump : public Worker
	{
	private:
		std::string filename;

	public:
		Dump(std::string name) { filename = name; }

		void process(WorkList *) override;
	};
}

#endif // DUMP_HEAD