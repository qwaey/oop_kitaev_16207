#include "replace.h"

command::Replace::Replace(std::string word0, std::string word1)
{
	word[0] = word0;
	word[1] = word1;
}

void command::Replace::process(WorkList *list)
{
	for (WorkList::iterator Iter = list->begin(); Iter != list->end(); Iter++)
	{
		size_t f = Iter->find(word[0]);

		if (f != std::string::npos)
		{
			Iter->replace(f, word[0].length(), word[1]);
		}
	}
}