#ifndef PARSER_HEAD
#define PARSER_HEAD

#include "lib.h"
#include "workFactory.h"
#include "worker.h"
#include "baseClass.h"

class Parser : public BaseParser
{
private:
	using ArgMap = std::map<std::string, int>;
	ArgMap argMap;

	using IdMap = std::map<int, std::pair<std::string, std::list<std::string> *>>;
	IdMap idMap;

	using WorkerList = std::list<std::pair<int, Worker *>>;
	WorkerList *ListConveyor;

	size_t NumOfLine = 0;

	void checkBlock(std::string, std::istringstream &, int);
	void checkConveyor(std::string, std::istringstream &);
	void checkSepChar(std::string, std::string);
	void checkSequence(std::string);

	void fileParsing(std::string, std::ifstream &);
	void parsDesc(std::string, std::ifstream &);
	void parsCsed(std::string, std::ifstream &);
	void parsline(std::ifstream &, std::string &);
	void parsNextPiece(std::string,  std::istringstream &, std::string &);

	int getCommandId(std::string, std::istringstream &);
	int getNumOfArgsCommand(std::string);
	int getIntId(std::string);

public:
	Parser();
	~Parser();

	void pars(std::string) override;
	WorkerList *getWorkerList() override;
};

#endif // PARSER_HEAD