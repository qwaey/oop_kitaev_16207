#include "factory.h"

Parser * Factory::getParser() 
{
	static std::auto_ptr<Parser> pars(new Parser);
	return pars.get();
}

Executor * Factory::getExecutor(BaseParser * pars) 
{
	static std::auto_ptr<Executor> exec(new Executor(*pars));
	return exec.get();
}