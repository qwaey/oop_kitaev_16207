#include "grep.h"

void command::Grep::process(WorkList *list)
{
	for (auto Iter = list->begin(); Iter != list->end(); )
	{
		if (Iter->find(word) != std::string::npos)
		{
			Iter++;
			continue;
		}

		auto oldIter = Iter;
		Iter++;
		list->erase(oldIter);
	}
}