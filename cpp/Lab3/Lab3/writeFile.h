#ifndef WRITEFILE_HEAD
#define WRITEFILE_HEAD

#include "lib.h"
#include "worker.h"

namespace command
{
	class WriteFile : public Worker
	{
	private:
		std::string filename;

	public:
		WriteFile(std::string name) { filename = name; }

		void process(WorkList *) override;
	};
}

#endif // WRITEFILE_HEAD