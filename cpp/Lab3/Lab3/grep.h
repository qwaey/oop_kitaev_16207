#ifndef GREP_HEAD
#define GREP_HEAD

#include "worker.h"

namespace command
{
	class Grep : public Worker
	{
	private:
		std::string word;

	public:
		Grep(std::string wrd) { word = wrd; }

		void process(WorkList *) override;
	};
}

#endif // GREP_HEAD