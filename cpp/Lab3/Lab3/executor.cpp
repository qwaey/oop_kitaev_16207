#include "executor.h"

Executor::Executor(BaseParser &pars)
	: parser(pars)
{
}

void Executor::run(std::string filename)
{
	parser.pars(filename);

	workerList = parser.getWorkerList();
	std::unique_ptr<WorkList> workList(new WorkList);

	for (auto Iter = workerList->begin(); Iter != workerList->end(); Iter++)
	{
		Iter->second->process(workList.get());
	}
}