package com.lab1.java;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class lab1 {
    public static void main(String[] args) {
        AtomicInteger sum = new AtomicInteger();
        String filename = args[0];
        String newFilename = filename + ".csv";
        StringBuilder strBuild = new StringBuilder();

        try(Writer out = new OutputStreamWriter(new FileOutputStream(newFilename))) {
            out.write(newFilename);
            Files.lines(Paths.get(filename))
                    .map(line -> line.split("\\W"))
                    .flatMap(Arrays::stream)
                    .filter(line -> !line.isEmpty())
                    .peek(val -> sum.getAndIncrement())
                    .collect(groupingBy(identity(), counting()))
                    .entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue())
                    .forEach((Map.Entry<String, Long> iter) -> {

                        try {
                            strBuild.append("\n").append(iter.getKey()).append(";").append(iter.getValue()).append(";").append(((float)iter.getValue() / sum.get() * 100)).append("%");
                            out.write(strBuild.toString());
                            strBuild.setLength(0);
                        }
                        catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
        catch(IOException e){
            System.err.println(e.getLocalizedMessage());
        }
    }
}