package view;

import factory.Updatable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.util.List;
import java.util.ArrayList;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewController implements Initializable {
    private static ViewController viewController = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        viewController = this;
    }

    public static ViewController getViewController(){
        return viewController;
    }


    @FXML private Text motorProd;
    private Integer motorNum = 0;
    synchronized public void incMotorProd(){
        ++motorNum;
        motorProd.setText(motorNum.toString());
    }

    @FXML private Text bodyProd;
    private Integer bodyNum = 0;
    synchronized public void incBodyProd(){
        ++bodyNum;
        bodyProd.setText(bodyNum.toString());
    }

    @FXML private Text accessoryProd;
    private Integer accessoryNum = 0;
    synchronized public void incAccessoryProd(){
        ++accessoryNum;
        accessoryProd.setText(accessoryNum.toString());
    }

    @FXML private Text autoProd;
    private Integer autoNum = 0;
    synchronized public void incAutoProd(){
        ++autoNum;
        autoProd.setText(autoNum.toString());
    }

    @FXML private Text soldAuto;
    private Integer soldAutoNum = 0;
    synchronized public void incSoldAuto(){
        ++soldAutoNum;
        soldAuto.setText(soldAutoNum.toString());
    }



    @FXML private ProgressBar barMotor;
    synchronized public void setBarMotor(double value){
        barMotor.setProgress(value);
    }

    @FXML private ProgressBar barBody;
    synchronized public void setBarBody(double value){
        barBody.setProgress(value);
    }

    @FXML private ProgressBar barAccessory;
    synchronized public void setBarAccessory(double value){
        barAccessory.setProgress(value);
    }

    @FXML private ProgressBar barAuto;
    synchronized public void setBarAuto(double value){
        barAuto.setProgress(value);
    }



    @FXML private Text motorStorage;
    synchronized public void setMotorStorage(String str){
        motorStorage.setText(str);
    }

    @FXML private Text bodyStorage;
    synchronized public void setBodyStorage(String str){
        bodyStorage.setText(str);
    }

    @FXML private Text accessoryStorage;
    synchronized public void setAccessoryStorage(String str){
        accessoryStorage.setText(str);
    }

    @FXML private Text autoStorage;
    synchronized public void setAutoStorage(String str){
        autoStorage.setText(str);
    }



    // Update motor performance slider
    @FXML private Slider sliderMotor;
    private List<Updatable> subscribersDragSliderMotor = new ArrayList<>();
    public void dragSliderMotor(MouseEvent mouseEvent) {
        subscribersDragSliderMotor.forEach(sub -> sub.update(sliderMotor.getValue()));
    }

    synchronized public void addListenerDragSliderMotor(Updatable listener){
        subscribersDragSliderMotor.add(listener);
    }


    // Update body performance slider
    @FXML private Slider sliderBody;
    private List<Updatable> subscribersDragSliderBody = new ArrayList<>();
    public void dragSliderBody(MouseEvent mouseEvent) {
        subscribersDragSliderBody.forEach(sub -> sub.update(sliderBody.getValue()));
    }

    synchronized public void addListenerDragSliderBody(Updatable listener){
        subscribersDragSliderBody.add(listener);
    }


    // Update accessory performance slider
    @FXML private Slider sliderAccessory;
    private List<Updatable> subscribersDragSliderAccessory = new ArrayList<>();
    public void dragSliderAccessory(MouseEvent mouseEvent) {
        subscribersDragSliderAccessory.forEach(sub -> sub.update(sliderAccessory.getValue()));
    }

    synchronized public void addListenerDragSliderAccessory(Updatable listener){
        subscribersDragSliderAccessory.add(listener);
    }


    // Update dealer performance slider
    @FXML private Slider sliderDealer;
    private List<Updatable> subscribersDragSliderDealer = new ArrayList<>();
    public void dragSliderDealer(MouseEvent mouseEvent) {
        subscribersDragSliderDealer.forEach(sub -> sub.update(sliderDealer.getValue()));
    }

    synchronized public void addListenerDragSliderDealer(Updatable listener){
        subscribersDragSliderDealer.add(listener);
    }
}
