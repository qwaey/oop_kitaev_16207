package factory;

import factory.detail.Auto;

import java.io.*;
import java.util.Date;

public class SalesLog {
    private Writer writer;

    SalesLog(Writer writer){
        this.writer = writer;
    }

    public void writeInLog(Auto auto){
        if (writer == null || auto == null){
            return;
        }

        final StringBuilder strBuild = new StringBuilder();
        Date date = new Date();
        strBuild.append("<").append(date.toString()).append(">:")
                .append(" Dealer:<").append(Thread.currentThread().getName()).append(">")
                .append(" Auto:<").append(auto.getIdentifier()).append(">")
                .append(" (Body:<").append(auto.getBody().getIdentifier()).append(">")
                .append(", Motor:<").append(auto.getMotor().getIdentifier()).append(">")
                .append(", Accessory:<").append(auto.getAccessory().getIdentifier())
                .append(">)\n");

        try {
            writer.write(strBuild.toString());
            writer.flush();
            //System.out.print(strBuild.toString()); //Вывод лога в консоль
            strBuild.setLength(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
