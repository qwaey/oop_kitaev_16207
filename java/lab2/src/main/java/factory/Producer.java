package factory;

import factory.storage.Storage;
import factory.supplier.Supplier;
import factory.updateView.UpdateDetailQuantity;
import java.util.List;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Producer implements Runnable, Updatable {
    private final int minManufacturingTime = 200;
    private final int maxManufacturingTime = 6000;
    private int manufacturingTime = maxManufacturingTime;

    private List<UpdateDetailQuantity> subscribersUpdateQuantity = new ArrayList<>();
    private Storage storage;
    private Supplier supplier;
    private Integer numDetail = 0;

    Producer(Storage newStorage, Supplier supplier) {
        this.storage = newStorage;
        this.supplier = supplier;
    }

    synchronized public void addListener(UpdateDetailQuantity listener){
        subscribersUpdateQuantity.add(listener);
    }

    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                while(manufacturingTime == maxManufacturingTime){
                    sleep(1000);
                }

                sleep(manufacturingTime);
                storage.putDetail(supplier.getDetail(numDetail));
                ++numDetail;
                subscribersUpdateQuantity.forEach(UpdateDetailQuantity::updateQuantity);
            }
            catch (InterruptedException e) {
                return;
            }
        }
    }

    private void setPerformance(int newPerformance) {
        if (newPerformance < 0 || newPerformance > 100) {
            return;
        }

        manufacturingTime = (100 - newPerformance) * (maxManufacturingTime - minManufacturingTime) / 100 + minManufacturingTime;
    }

    private int getPerformance() {
        return 100 * (manufacturingTime - minManufacturingTime) / (maxManufacturingTime - minManufacturingTime);
    }

    public void update(Double perf){
        setPerformance((int)perf.doubleValue());
    }
}