package factory.storage;

import factory.detail.Detail;
import factory.updateView.UpdateStorage;

import java.util.Queue;
import java.util.List;
import java.util.ArrayList;
import java.util.ArrayDeque;

public class Storage {
    private int capacity;
    private Queue<Detail> storage = new ArrayDeque<>();
    private List<UpdateStorage> subscribersUpdateStorage = new ArrayList<>();

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    synchronized public void addListener(UpdateStorage listener){
        subscribersUpdateStorage.add(listener);
    }

    synchronized public void putDetail(Detail detail) {
        if (detail == null){
            return;
        }

        while(storage.size() == capacity) {
            try {
                wait();
            }
            catch (InterruptedException e) {
                return;
            }
        }

        storage.add(detail);
        subscribersUpdateStorage.forEach(sub -> sub.updateStorageFullness(storage.size(), capacity));
        notifyAll();
    }

    synchronized public Detail getDetail() throws NullPointerException {
        while (storage.size() == 0){
            try {
                wait();
            }
            catch (InterruptedException e) {
                return null;
            }
        }

        subscribersUpdateStorage.forEach(sub -> sub.updateStorageFullness(storage.size() - 1, capacity));
        notifyAll();
        return storage.poll();
    }

    synchronized public boolean isEmpty(){
        return storage.isEmpty();
    }

    synchronized public int fullness(int sizeQueue){
        return (100 * (storage.size() + sizeQueue) / capacity);
    }

    synchronized public void listen() throws InterruptedException {
        wait();
    }
}