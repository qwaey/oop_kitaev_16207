package factory.storage;

import factory.task.WorkerTask;
import factory.updateView.UpdateDetailQuantity;
import threadPool.ThreadPool;

import java.util.ArrayList;
import java.util.List;

public class StorageController implements Runnable {
    private List<UpdateDetailQuantity> subscribersUpdateQuantity = new ArrayList<>();
    private Storage storageAuto;
    private Storage storageMotor;
    private Storage storageBody;
    private Storage storageAccessory;
    private ThreadPool workers;
    private Integer numDetail = 0;
    private final int percent = 75;

    public StorageController(ThreadPool workers, Storage storageAuto, Storage storageMotor, Storage storageBody, Storage storageAccessory){
        this.workers = workers;
        this.storageAuto = storageAuto;
        this.storageMotor = storageMotor;
        this.storageBody = storageBody;
        this.storageAccessory = storageAccessory;
    }

    public void addListener(UpdateDetailQuantity listener){
        subscribersUpdateQuantity.add(listener);
    }

    public void run(){
        while(!Thread.currentThread().isInterrupted()){
            while(storageAuto.fullness(workers.sizeQueueProblem()) < percent) {
                ++numDetail;
                workers.putProblem(new WorkerTask("a" + numDetail.toString(), subscribersUpdateQuantity, storageAuto, storageMotor.getDetail(), storageBody.getDetail(), storageAccessory.getDetail()));
            }

            try {
                storageAuto.listen();
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
