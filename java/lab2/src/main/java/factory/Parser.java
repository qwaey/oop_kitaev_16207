package factory;

import java.io.*;
import java.lang.String;

import java.util.Properties;

/*****************
 StorageBodySize=100
 StorageMotorSize=100
 StorageAccessorySize=100
 StorageAutoSize=100
 AccessorySuppliers=5
 Workers=10
 Dealers=20
 LogSale=false
 *****************/

class Parser {
    private Properties props;

    Parser(String file) {
        try {
            props = new Properties();
            props.load(new FileInputStream(new File(file)));
        }
        catch(FileNotFoundException e){
            System.err.println("File with name: " + e.getLocalizedMessage() + " not found");
        }
        catch(IOException e){
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
    }

    int getStorageBodySize(){
        return Integer.valueOf(props.getProperty("StorageBodySize", "1"));
    }

    int getStorageMotorSize(){
        return Integer.valueOf(props.getProperty("StorageMotorSize", "1"));
    }

    int getStorageAccessorySize(){
        return Integer.valueOf(props.getProperty("StorageAccessorySize", "1"));
    }

    int getStorageAutoSize(){
        return Integer.valueOf(props.getProperty("StorageAutoSize", "1"));
    }

    int getAccessorySuppliers(){
        return Integer.valueOf(props.getProperty("AccessorySuppliers", "1"));
    }

    int getWorkers(){
        return Integer.valueOf(props.getProperty("Workers", "1"));
    }

    int getDealers(){
        return Integer.valueOf(props.getProperty("Dealers", "1"));
    }

    boolean getLogSale(){
        return Boolean.valueOf(props.getProperty("LogSale", "false"));
    }
}
