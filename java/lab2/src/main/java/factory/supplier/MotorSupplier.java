package factory.supplier;

import factory.detail.Motor;
import factory.detail.Detail;

public class MotorSupplier extends Supplier {
    @Override
    public Detail getDetail(Integer id) {
        return new Motor("m" + id.toString());
    }
}