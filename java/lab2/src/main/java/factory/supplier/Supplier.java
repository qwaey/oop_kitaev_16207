package factory.supplier;

import factory.detail.Detail;

abstract public class Supplier {
    public abstract Detail getDetail(Integer id);
}