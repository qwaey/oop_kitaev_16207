package factory.supplier;

import factory.detail.Body;
import factory.detail.Detail;

public class BodySupplier extends Supplier {
    @Override
    public Detail getDetail(Integer id) {
        return new Body("b" + id.toString());
    }
}