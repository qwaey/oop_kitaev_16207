package factory.supplier;

import factory.detail.Accessory;
import factory.detail.Detail;

public class AccessorySupplier extends Supplier {
    @Override
    public Detail getDetail(Integer id) {
        return new Accessory(Thread.currentThread().getName() + "a" + id.toString());
    }
}