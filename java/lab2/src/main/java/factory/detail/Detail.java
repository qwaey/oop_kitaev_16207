package factory.detail;

public abstract class Detail {
    private String identifier;

    public Detail(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier(){
        return identifier;
    }
}


