package factory.detail;

final public class Auto extends Detail {
    private Detail motor;
    private Detail body;
    private Detail accessory;

    public Auto(String identifier, Detail motor, Detail body, Detail accessory) {
        super(identifier);
        this.motor = motor;
        this.body = body;
        this.accessory = accessory;
    }

    public Detail getMotor() {
        return motor;
    }

    public Detail getBody() {
        return body;
    }

    public Detail getAccessory() {
        return accessory;
    }
}