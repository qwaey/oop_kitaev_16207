package factory.detail;

final public class Accessory extends Detail {
    public Accessory(String identifier) {
        super(identifier);
    }
}