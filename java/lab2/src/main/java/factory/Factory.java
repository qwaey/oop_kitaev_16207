package factory;

import factory.storage.Storage;
import factory.storage.StorageController;
import factory.supplier.AccessorySupplier;
import factory.supplier.BodySupplier;
import factory.supplier.MotorSupplier;
import factory.updateView.*;
import threadPool.ThreadPool;
import view.ViewController;

import java.util.*;
import java.io.*;

public class Factory {
    private String filename;
    private Writer writer;
    private ViewController viewController;
    private List<Thread> allThreads = new ArrayList<>();

    public Factory(String filename, ViewController viewController){
        this.filename = filename;
        this.viewController = viewController;
    }

    public void interrupt(){
        for (Thread threads: allThreads) {
            threads.interrupt();
        }
    }

    public void start() {
        // Парсировка файла конфигурации
        Parser parser = new Parser(filename);

        // Создание складов
        Storage motorStorage = new Storage(parser.getStorageMotorSize());
        Storage bodyStorage = new Storage(parser.getStorageBodySize());
        Storage accessoryStorage = new Storage(parser.getStorageAccessorySize());
        Storage autoStorage = new Storage(parser.getStorageAutoSize());

        // (Подписываем ViewController на склады)
        motorStorage.addListener(new UpdateMotorStorage());
        bodyStorage.addListener(new UpdateBodyStorage());
        accessoryStorage.addListener(new UpdateAccessoryStorage());
        autoStorage.addListener(new UpdateAutoStorage());

        // Создание пула рабочих
        ThreadPool workersPool = new ThreadPool(parser.getWorkers(), allThreads);

        // Создание контроллера склада авто
        StorageController storageController = new StorageController(workersPool, autoStorage, motorStorage, bodyStorage, accessoryStorage);
        storageController.addListener(new UpdateAutoQuantity());
        allThreads.add(new Thread(storageController));

        // Создание поставщиков
        Producer motorProducer = new Producer(motorStorage, new MotorSupplier());
        motorProducer.addListener(new UpdateMotorQuantity());
        ViewController.getViewController().addListenerDragSliderMotor(motorProducer);
        allThreads.add(new Thread(motorProducer));

        Producer bodyProducer = new Producer(bodyStorage, new BodySupplier());
        bodyProducer.addListener(new UpdateBodyQuantity());
        ViewController.getViewController().addListenerDragSliderBody(bodyProducer);
        allThreads.add(new Thread(bodyProducer));

        for (int i = 0; i < parser.getAccessorySuppliers(); ++i){
            Producer accessoryProducer = new Producer(accessoryStorage, new AccessorySupplier());
            accessoryProducer.addListener(new UpdateAccessoryQuantity());
            ViewController.getViewController().addListenerDragSliderAccessory(accessoryProducer);
            Thread thread = new Thread(accessoryProducer);
            thread.setName(((Integer)(i)).toString());
            allThreads.add(thread);
        }

        if (parser.getLogSale()){
            try {
                writer = new OutputStreamWriter(new FileOutputStream("sold_auto.log"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Создание потребителей
        for (int i = 0; i < parser.getDealers(); ++i){
            Dealer dealer = new Dealer(autoStorage, writer);
            dealer.addListener(new UpdateSoldAutoQuantity());
            viewController.addListenerDragSliderDealer(dealer);
            Thread thread = new Thread(dealer);
            thread.setName(((Integer)(i)).toString());
            allThreads.add(thread);
        }

        // Запуск потоков
        for (Thread threads: allThreads) {
            threads.start();
        }
    }
}
