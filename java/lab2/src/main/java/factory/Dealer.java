package factory;

import factory.detail.Auto;
import factory.detail.Detail;
import factory.storage.Storage;
import factory.updateView.UpdateDetailQuantity;

import static java.lang.Thread.sleep;
import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class Dealer implements Runnable, Updatable {
    final private int minManufacturingTime = 100;
    final private int maxManufacturingTime = 8000;
    private int manufacturingTime = maxManufacturingTime;
    private List<UpdateDetailQuantity> subscribersUpdateQuantity = new ArrayList<>();
    private Storage storage;
    private SalesLog logger;

    Dealer(Storage newStorage, Writer writer){
        this.storage = newStorage;
        this.logger = new SalesLog(writer);
    }

    public void addListener(UpdateDetailQuantity listener){
        subscribersUpdateQuantity.add(listener);
    }

    public void run(){
        while(!Thread.currentThread().isInterrupted()){
            try {
                while(manufacturingTime == maxManufacturingTime){
                    sleep(1000);
                }

                sleep(manufacturingTime);
                Detail auto = storage.getDetail();
                subscribersUpdateQuantity.forEach(UpdateDetailQuantity::updateQuantity);
                logger.writeInLog((Auto)auto);
            }
            catch (InterruptedException e) {
                return;
            }
        }
    }

    private void setPerformance(int newPerformance) {
        if (newPerformance < 0 || newPerformance > 100) {
            return;
        }

        manufacturingTime = (100 - newPerformance) * (maxManufacturingTime - minManufacturingTime) / 100 + minManufacturingTime;
    }

    public int getPerformance() {
        return 100 * (manufacturingTime - minManufacturingTime) / (maxManufacturingTime - minManufacturingTime);
    }

    public void update(Double perf){
        setPerformance((int)perf.doubleValue());
    }
}
