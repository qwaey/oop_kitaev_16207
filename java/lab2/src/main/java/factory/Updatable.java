package factory;

public interface Updatable {
    void update(Double o);
}
