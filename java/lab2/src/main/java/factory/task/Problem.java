package factory.task;

public interface Problem {
    void resolve();
}
