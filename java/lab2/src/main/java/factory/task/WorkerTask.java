package factory.task;

import factory.detail.Detail;
import factory.detail.Auto;
import factory.storage.Storage;
import factory.updateView.UpdateDetailQuantity;

import java.util.List;

public class WorkerTask implements Problem {
    private Detail motor;
    private Detail body;
    private Detail accessory;
    private String id;
    private Storage storageAuto;
    private List<UpdateDetailQuantity> subscribersUpdateQuantity;

    public WorkerTask(String id, List<UpdateDetailQuantity> subscribersUpdateQuantity, Storage storageAuto, Detail motor, Detail body, Detail accessory){
        this.storageAuto = storageAuto;
        this.subscribersUpdateQuantity = subscribersUpdateQuantity;
        this.id = id;
        this.motor = motor;
        this.body = body;
        this.accessory = accessory;
    }

    public void resolve(){
        storageAuto.putDetail(new Auto(id, motor, body, accessory));
        subscribersUpdateQuantity.forEach(UpdateDetailQuantity::updateQuantity);
    }
}
