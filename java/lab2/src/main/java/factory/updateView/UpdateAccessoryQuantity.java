package factory.updateView;

import view.ViewController;

public class UpdateAccessoryQuantity extends UpdateDetailQuantity {
    @Override
    synchronized public void updateQuantity() {
        ViewController.getViewController().incAccessoryProd();
    }
}
