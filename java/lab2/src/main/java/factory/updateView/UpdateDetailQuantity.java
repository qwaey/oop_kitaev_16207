package factory.updateView;

public abstract class UpdateDetailQuantity {
    public abstract void updateQuantity();
}
