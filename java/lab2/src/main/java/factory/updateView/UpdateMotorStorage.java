package factory.updateView;

import view.ViewController;

public class UpdateMotorStorage extends UpdateStorage {
    @Override
    synchronized public void updateStorageFullness(Integer curFullness, Integer maxFullness) {
        ViewController.getViewController().setMotorStorage(curFullness.toString() + "/" + maxFullness.toString());
        ViewController.getViewController().setBarMotor(curFullness.doubleValue() / maxFullness.doubleValue());
    }
}
