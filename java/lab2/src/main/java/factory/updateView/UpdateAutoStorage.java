package factory.updateView;

import view.ViewController;

public class UpdateAutoStorage extends UpdateStorage {
    @Override
    synchronized public void updateStorageFullness(Integer curFullness, Integer maxFullness) {
        ViewController.getViewController().setAutoStorage(curFullness.toString() + "/" + maxFullness.toString());
        ViewController.getViewController().setBarAuto(curFullness.doubleValue() / maxFullness.doubleValue());
    }
}
