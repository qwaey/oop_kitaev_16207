package factory.updateView;

import view.ViewController;

public class UpdateMotorQuantity extends UpdateDetailQuantity {
    @Override
    synchronized public void updateQuantity() {
        ViewController.getViewController().incMotorProd();
    }
}
