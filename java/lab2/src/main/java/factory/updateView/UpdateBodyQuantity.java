package factory.updateView;

import view.ViewController;

public class UpdateBodyQuantity extends UpdateDetailQuantity {
    @Override
    synchronized public void updateQuantity() {
        ViewController.getViewController().incBodyProd();
    }
}
