package factory.updateView;

import view.ViewController;

public class UpdateAutoQuantity extends UpdateDetailQuantity {
    @Override
    synchronized public void updateQuantity() {
        ViewController.getViewController().incAutoProd();
    }
}
