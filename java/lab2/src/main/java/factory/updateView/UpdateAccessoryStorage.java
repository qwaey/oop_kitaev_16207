package factory.updateView;

import view.ViewController;

public class UpdateAccessoryStorage extends UpdateStorage {
    @Override
    synchronized public void updateStorageFullness(Integer curFullness, Integer maxFullness) {
        ViewController.getViewController().setAccessoryStorage(curFullness.toString() + "/" + maxFullness.toString());
        ViewController.getViewController().setBarAccessory(curFullness.doubleValue() / maxFullness.doubleValue());
    }
}
