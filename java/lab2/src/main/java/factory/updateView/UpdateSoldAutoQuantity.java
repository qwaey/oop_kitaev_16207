package factory.updateView;

import view.ViewController;

public class UpdateSoldAutoQuantity extends UpdateDetailQuantity {
    @Override
    synchronized public void updateQuantity() {
        ViewController.getViewController().incSoldAuto();
    }
}
