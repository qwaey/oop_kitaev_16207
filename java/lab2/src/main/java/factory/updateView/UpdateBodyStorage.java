package factory.updateView;

import view.ViewController;

public class UpdateBodyStorage extends UpdateStorage {
    @Override
    synchronized public void updateStorageFullness(Integer curFullness, Integer maxFullness) {
        ViewController.getViewController().setBodyStorage(curFullness.toString() + "/" + maxFullness.toString());
        ViewController.getViewController().setBarBody(curFullness.doubleValue() / maxFullness.doubleValue());
    }
}
