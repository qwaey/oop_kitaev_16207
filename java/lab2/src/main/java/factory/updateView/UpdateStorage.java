package factory.updateView;

public abstract class UpdateStorage {
    public abstract void updateStorageFullness(Integer cur, Integer max);
}