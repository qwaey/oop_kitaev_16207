import factory.Factory;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import view.ViewController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.String;

public class Main extends Application{

    @Override
    public void start(Stage primaryStage) {
        try{
            String filename = "config.ini";
            Parent root = FXMLLoader.load(getClass().getResource("scene.fxml"));

            Factory factory = new Factory(filename, ViewController.getViewController());
            factory.start();

            Scene scene = new Scene(root, 700, 360);

            primaryStage.setResizable(false);
            primaryStage.setTitle("Thread factory");
            primaryStage.setScene(scene);
            primaryStage.show();

            primaryStage.setOnCloseRequest(EventHandler -> {
                factory.interrupt();
                System.exit(0);
            });
        }
        catch(FileNotFoundException e){
            System.err.println("File with name: " + e.getLocalizedMessage() + " not found");
        }
        catch(IOException e){
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}