package threadPool;

import factory.task.Problem;

import java.util.List;
import java.util.Queue;
import java.util.ArrayDeque;

public class ThreadPool {
    private Queue<Problem> queueProblem = new ArrayDeque<>();

    public ThreadPool(int workersNum, List<Thread> allThreads){
        for (int i = 0; i < workersNum; ++i){
            Thread thread = new Thread(new Worker(this));

            if (allThreads != null){
                allThreads.add(thread);
            }
        }
    }

    synchronized public Problem getProblem() {
        while(queueProblem.size() == 0){
            try {
                wait();
            }
            catch (InterruptedException e) {
                return null;
            }
        }

        notify();
        return queueProblem.poll();
    }

    synchronized public void putProblem(Problem newWorkerTask){
        queueProblem.add(newWorkerTask);
        notify();
    }

    synchronized public int sizeQueueProblem(){
        return queueProblem.size();
    }
}
