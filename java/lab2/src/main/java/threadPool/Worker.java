package threadPool;

import factory.task.Problem;

public class Worker implements Runnable {
    private ThreadPool threadPool;

    Worker(ThreadPool threadPool){
        this.threadPool = threadPool;
    }

    public void run(){
        while(!Thread.currentThread().isInterrupted()){
            Problem task = threadPool.getProblem();

            if (task != null){
                task.resolve();
            }
        }
    }
}
