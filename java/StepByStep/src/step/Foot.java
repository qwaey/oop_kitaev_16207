package step;

import static java.lang.Thread.sleep;

public class Foot implements Runnable {
    private Foot otherFoot;
    private boolean active;
    private String name;

    Foot(boolean active, String name){
        this.active = active;
        this.name = name;
    }

    public void addOtherFoot(Foot foot){
        this.otherFoot = foot;
    }

    synchronized public void notifyOtherFoot(){
        otherFoot.notifyMe();
    }

    synchronized public void notifyMe(){
        active = true;
        notify();
    }

    @Override
    synchronized public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                if (active){
                    sleep(1000);
                    System.out.println(name);
                    notifyOtherFoot();
                    active = false;
                } else {
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
