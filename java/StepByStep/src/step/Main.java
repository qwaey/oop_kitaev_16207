package step;

public class Main {
    public static void main(String[] args) {
        Foot leftFoot = new Foot(true, "left");
        Foot middleFoot = new Foot(false, "middle");
        Foot rightFoot = new Foot(false, "right");

        leftFoot.addOtherFoot(middleFoot);
        middleFoot.addOtherFoot(rightFoot);
        rightFoot.addOtherFoot(leftFoot);

        Thread leftThread = new Thread(leftFoot);
        Thread middleThread = new Thread(middleFoot);
        Thread rightThread = new Thread(rightFoot);

        rightThread.start();
        middleThread.start();
        leftThread.start();
    }
}
