package step;
import java.util.concurrent.*;

import static java.lang.Thread.sleep;

public class Leg<T> implements Runnable{
private Semaphore semaphore;
private Thread thread;
private String name;

    public Leg(Semaphore semaphore, String name) {
        this.semaphore = semaphore;
        this.name = name;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){


            try {
                semaphore.acquire();
                sleep(1000);
                System.out.println(name);
                semaphore.release();
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}