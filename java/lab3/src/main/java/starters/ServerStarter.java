package starters;

import com.company.server.Server;

public class ServerStarter {
    public static void main(String[] args) {
        (new Server()).launch();
    }
}
