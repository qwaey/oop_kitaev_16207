package starters;

import com.company.client.Client;
import com.company.client.ClientParser;
import com.view.LoginSceneViewDiskRotation;
import com.view.SceneController;
import javafx.application.Application;
import javafx.stage.Stage;
import java.lang.String;

public class ClientStarter extends Application{
    @Override
    public void start(Stage primaryStage) {
        SceneController sceneController = new SceneController(primaryStage);
        sceneController.setSceneLogin();

        LoginSceneViewDiskRotation diskRotation = new LoginSceneViewDiskRotation();
        diskRotation.start();

        Client client = new Client(new ClientParser());
        Thread clientThread = new Thread(client);
        clientThread.start();

        primaryStage.setOnCloseRequest(EventHandler -> {
            diskRotation.interrupt();
            client.interruptUser();
            clientThread.interrupt();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}