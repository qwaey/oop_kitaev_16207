package com.company.listener;

import org.w3c.dom.Document;

public interface ListDoc {
    void putDocument(Document document);
}
