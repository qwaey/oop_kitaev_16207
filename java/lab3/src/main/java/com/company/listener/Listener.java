package com.company.listener;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import java.util.List;
import java.util.ArrayList;

public class Listener implements Runnable {
    private List<ListDoc> subscribersList = new ArrayList<>();
    private Socket socket;

    public Listener(Socket socket){
        this.socket = socket;
    }

    synchronized public void addListener(ListDoc listener){
        subscribersList.add(listener);
    }

    synchronized public void run(){
        Thread.currentThread().setName("LISTENER");
        try {
            while(!Thread.currentThread().isInterrupted()){
                DataInputStream reader = new DataInputStream(socket.getInputStream());

                int size = reader.readInt();

                byte xml[] = new byte[size];
                reader.read(xml, 0, size);
                ByteArrayInputStream byteInputMessage = new ByteArrayInputStream(xml);

                Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(byteInputMessage);
                document.getDocumentElement().normalize();

                System.out.println("<<<<====" + (new String(xml)));
                subscribersList.forEach(sub -> sub.putDocument(document));
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            Thread.currentThread().interrupt();
        }
    }
}
