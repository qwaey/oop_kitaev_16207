package com.company.server;

import com.company.client.SocketException;
import com.company.listener.ListDoc;
import com.company.server.command.CommandFactory;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Sender implements Runnable, ListDoc {
    private User user;
    private Server server;

    public Sender(Server server, User user){
        this.user = user;
        this.server = server;
    }

    @Override
    public void putDocument(Document document) {
        user.putTask(CommandFactory.getQueueTask(server, user, document));
    }

    public void run(){
        try {
            DataOutputStream writer = new DataOutputStream(user.getSocket().getOutputStream());

            while(!Thread.currentThread().isInterrupted()){
                ByteArrayOutputStream byteOutputMessage = new ByteArrayOutputStream();
                user.getTask().resolve(writer, byteOutputMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            return;
        }
    }
}