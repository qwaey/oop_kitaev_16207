package com.company.server.command;

import com.company.server.Server;
import com.company.task.ManyTask;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import com.company.server.User;
import com.company.XmlElement;
import org.w3c.dom.Document;

import java.util.ArrayDeque;
import java.util.Queue;

public class CommandMessage extends Command {

    @Override
    public Queue<SolvableTask> getQueueTask(Server server, User user, Document document) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        String session = document.getElementsByTagName("session").item(0).getTextContent();
        String message = document.getElementsByTagName("message").item(0).getTextContent();

        if (server.containsId(session)){
            // Рассылка сообщения всем пользователям
            //****** ответ пользователю
            XmlElement success = new XmlElement("success");

            taskQueue.add(new SingleTask(success.generateDocument()));

            //****** пересылка
            XmlElement command = new XmlElement("event", "name", "message");
            XmlElement comMessage = new XmlElement("message", message);
            XmlElement name = new XmlElement("name", user.getName());
            command.appendChild(comMessage);
            command.appendChild(name);

            ManyTask manyTask = new ManyTask(command.generateDocument(), user.getId());
            server.getUserStream().forEach(manyTask::addListener);
            taskQueue.add(manyTask);
        } else {
            //****** ошибка, сессия не найдена
            XmlElement error = new XmlElement("error");
            XmlElement comMessage = new XmlElement("message", "incorrect session id");
            error.appendChild(comMessage);

            taskQueue.add(new SingleTask(error.generateDocument()));
        }

        return taskQueue;
    }
}
