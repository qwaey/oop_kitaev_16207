package com.company.server.command;

import com.company.server.Server;
import com.company.task.ManyTask;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import com.company.server.User;
import com.company.XmlElement;
import org.w3c.dom.Document;

import java.util.Queue;
import java.util.ArrayDeque;

public class CommandLogout extends Command {

    @Override
    public Queue<SolvableTask> getQueueTask(Server server, User user, Document document) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        String session = document.getElementsByTagName("session").item(0).getTextContent();

        if (server.containsId(session)) {
            //****** отчёт об успешном отключении
            server.deleteId(session);
            XmlElement success = new XmlElement("success");

            taskQueue.add(new SingleTask(success.generateDocument()));

            //****** рассылаем всем пользователям сообщение об отключении
            XmlElement event = new XmlElement("event", "name", "userlogout");
            XmlElement name = new XmlElement("name", user.getName());
            event.appendChild(name);

            ManyTask manyTask = new ManyTask(event.generateDocument(), user.getId());
            server.getUserStream().forEach(manyTask::addListener);
            taskQueue.add(manyTask);
        } else {
            //****** ошибка отключения, id сессии не найден
            XmlElement error = new XmlElement("error");
            XmlElement message = new XmlElement("message", "incorrect session id");
            error.appendChild(message);

            taskQueue.add(new SingleTask(error.generateDocument()));
        }

        return taskQueue;
    }
}
