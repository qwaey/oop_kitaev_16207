package com.company.server.command;

import com.company.server.Server;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import com.company.server.User;
import com.company.XmlElement;
import org.w3c.dom.Document;

import java.util.Queue;
import java.util.ArrayDeque;


public class CommandList extends Command {
    @Override
    public Queue<SolvableTask> getQueueTask(Server server, User user, Document document) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        String session = document.getElementsByTagName("session").item(0).getTextContent();

        if (server.containsId(session)){
            //****** формирование xml списка пользователей
            XmlElement success = new XmlElement("success");
            XmlElement listusers = new XmlElement("listusers");

            server.getUserStream().forEach(userIter -> {
                XmlElement userN = new XmlElement("user");
                XmlElement name = new XmlElement("name", userIter.getName());
                XmlElement type = new XmlElement("type", userIter.getChatClientType());
                userN.appendChild(name);
                userN.appendChild(type);
                listusers.appendChild(userN);
            });

            success.appendChild(listusers);
            taskQueue.add(new SingleTask(success.generateDocument()));
        }else{
            //****** отказ в выдаче списка пользователей
            XmlElement error = new XmlElement("error");
            XmlElement message = new XmlElement("message", "incorrect session id");
            error.appendChild(message);
            taskQueue.add(new SingleTask(error.generateDocument()));
        }

        return taskQueue;
    }
}
