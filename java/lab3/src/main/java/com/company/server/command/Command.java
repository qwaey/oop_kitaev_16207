package com.company.server.command;

import com.company.server.Server;
import com.company.task.SolvableTask;
import java.util.Queue;
import com.company.server.User;
import org.w3c.dom.Document;

abstract public class Command {
    abstract public Queue<SolvableTask> getQueueTask(Server server, User user, Document document);
}