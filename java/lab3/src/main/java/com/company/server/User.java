package com.company.server;

import com.company.listener.Listener;
import com.company.task.SingleTaskAdd;
import com.company.task.SolvableTask;

import java.net.Socket;
import java.util.*;

public class User implements SingleTaskAdd {
    private Thread senderThread;
    private Thread listenerThread;

    private String uniqueId;
    private String name;
    private String chatClientType;
    private Queue<SolvableTask> taskQueue = new ArrayDeque<>();
    private Socket socket;

    User(Server server, String uniqueId, Socket socket) {
        this.uniqueId = uniqueId;
        this.socket = socket;

        Sender sender = new Sender(server, this);
        Listener listener = new Listener(socket);
        listener.addListener(sender);

        senderThread = new Thread(sender);
        senderThread.start();
        listenerThread = new Thread(listener);
        listenerThread.start();
    }

    @Override
    synchronized public void putSingleTask(SolvableTask task) {
        if ((name != null)){
            taskQueue.add(task);
            notify();
        }
    }

    @Override
    public String getId() {
        return uniqueId;
    }

    synchronized public void putTask(Queue<SolvableTask> task) {
        taskQueue.addAll(task);
        notify();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChatClientType() {
        return chatClientType;
    }

    public void setChatClientType(String chatClientType) {
        this.chatClientType = chatClientType;
    }

    public String getName() {
        return name;
    }

    synchronized public SolvableTask getTask() {
        while (taskQueue.size() == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                return null;
            }
        }

        return taskQueue.poll();
    }

    synchronized public Socket getSocket() {
        return socket;
    }

    public void interrupt(){
        senderThread.interrupt();
        listenerThread.interrupt();
    }
}
