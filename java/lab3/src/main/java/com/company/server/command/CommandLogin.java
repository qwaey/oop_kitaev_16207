package com.company.server.command;

import com.company.server.Server;
import com.company.task.ManyTask;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import java.util.Queue;
import java.util.ArrayDeque;
import com.company.server.User;
import com.company.XmlElement;

import org.w3c.dom.Document;

public class CommandLogin extends Command {

    @Override
    public Queue<SolvableTask> getQueueTask(Server server, User user, Document document) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        String name = document.getElementsByTagName("name").item(0).getTextContent();
        String chatClientType = document.getElementsByTagName("type").item(0).getTextContent();

        user.setChatClientType(chatClientType);

        if (server.containsName(name)){
            // Отказ в регистрации
            //****** Сообщение об ошибке
            XmlElement answer = new XmlElement("error");
            XmlElement message = new XmlElement("message", "User with that nick already exists");
            answer.appendChild(message);

            taskQueue.add(new SingleTask(answer.generateDocument()));
        } else {
            // Успешная регистрация
            user.setName(name);

            //****** Сообщение об успехе
            XmlElement answer = new XmlElement("success");
            XmlElement message = new XmlElement("session", user.getId());
            answer.appendChild(message);

            taskQueue.add(new SingleTask(answer.generateDocument()));

            //****** Сообщение всем о подключении нового пользователя
            XmlElement event = new XmlElement("event", "name", "userlogin");
            XmlElement nickname = new XmlElement("name", name);
            event.appendChild(nickname);

            ManyTask manyTask = new ManyTask(event.generateDocument(), user.getId());
            server.getUserStream().forEach(manyTask::addListener);
            taskQueue.add(manyTask);
        }

        return taskQueue;
    }
}
