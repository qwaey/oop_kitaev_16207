package com.company.server.command;

import com.company.task.SolvableTask;
import java.util.Queue;
import com.company.server.Server;
import com.company.server.User;
import org.w3c.dom.Document;
import java.util.Map;
import java.util.HashMap;

public class CommandFactory {
    private static Map<String, Command> commandMap = new HashMap<>();

    public static void addCommandInMap(String name, Command command){
        commandMap.put(name, command);
    }

    public static Queue<SolvableTask> getQueueTask(Server server, User user, Document document){
        String commandName = document.getDocumentElement().getAttribute("name");
        return commandMap.get(commandName).getQueueTask(server, user, document);
    }
}
