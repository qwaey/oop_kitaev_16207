package com.company.server;

import com.company.server.command.*;

import java.lang.*;
import java.io.*;
import java.util.*;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Stream;

public class Server {
    private Map<String, User> userBase = new HashMap<>();

    public Server(){
        CommandFactory.addCommandInMap("list", new CommandList());
        CommandFactory.addCommandInMap("login", new CommandLogin());
        CommandFactory.addCommandInMap("logout", new CommandLogout());
        CommandFactory.addCommandInMap("message", new CommandMessage());
    }

    public void launch() {
        try (ServerSocket serverSocket = new ServerSocket(777)) {
            while (!Thread.currentThread().isInterrupted()) {
                Socket clientSocket = serverSocket.accept();

                InetAddress inetAddress = clientSocket.getInetAddress();
                String uniqueId = generateUniqueId();

                System.out.println("Connect: " + inetAddress.toString() + "; ID: " + uniqueId + ";");

                userBase.put(uniqueId, new User(this, uniqueId, clientSocket));
            }

            userBase.values().forEach(User::interrupt);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateUniqueId(){
        Integer iter = 0;
        while(userBase.containsKey(iter.toString())){
            ++iter;
        }
        
        return iter.toString();
    }

    synchronized public Stream<User> getUserStream(){
         return userBase.values().stream();
    }

    synchronized public boolean containsName(String name){
        for (User user: userBase.values()) {
            if (user.getName() != null){
                if (user.getName().equals(name)) {
                    return true;
                }
            }
        }

        return false;
    }

    synchronized public boolean containsId(String id){
        return userBase.containsKey(id);
    }

    synchronized public void deleteId(String id){
        userBase.remove(id);
    }
}
