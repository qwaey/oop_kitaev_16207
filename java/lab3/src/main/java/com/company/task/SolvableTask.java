package com.company.task;

import com.company.client.SocketException;

import java.io.*;

public interface SolvableTask {
    void resolve(DataOutputStream writer, ByteArrayOutputStream output) throws SocketException;
}
