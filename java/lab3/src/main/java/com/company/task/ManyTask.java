package com.company.task;

import org.w3c.dom.Document;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import java.util.List;
import java.util.ArrayList;

public class ManyTask implements SolvableTask {
    private Document toDo;
    private List<SingleTaskAdd> subscribersList = new ArrayList<>();
    private String idExcluded;

    public ManyTask(Document document, String idExcluded){
        this.toDo = document;
        this.idExcluded = idExcluded;
    }

    public void addListener(SingleTaskAdd sub){
        subscribersList.add(sub);
    }

    @Override
    public void resolve(DataOutputStream writer, ByteArrayOutputStream byteOutput) {
        subscribersList.forEach(sub -> {
            if (!idExcluded.equals(sub.getId())){
                sub.putSingleTask(new SingleTask(toDo));
            }
        });
    }
}
