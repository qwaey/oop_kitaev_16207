package com.company.task;

import java.io.*;

import com.company.client.SocketException;
import org.w3c.dom.Document;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class SingleTask implements SolvableTask {
    private Document toDo;

    public SingleTask(Document document){
        toDo = document;
    }

    @Override
    public void resolve(DataOutputStream writer, ByteArrayOutputStream byteOutput) throws SocketException {
        try {
            TransformerFactory.newInstance()
                    .newTransformer()
                    .transform(new DOMSource(toDo), new StreamResult(byteOutput));

            System.out.println("====>>>>" + byteOutput.toString());

            writer.writeInt(byteOutput.size());
            writer.write(byteOutput.toByteArray());
            writer.flush();
        } catch (TransformerException | IOException e) {
            throw new SocketException("Remote server not responding :(");
        }
    }
}
