package com.company.task;

public interface SingleTaskAdd {
    void putSingleTask(SolvableTask task);
    String getId();
}
