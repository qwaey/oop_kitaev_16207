package com.company.client.clientEvent;

import com.view.ChatSceneView;
import javafx.application.Platform;
import org.w3c.dom.Node;

public class OnlineMessageEvent {
    public static void addItem(Node user, boolean my){
        String name = "";
        String message = "";

        for(int j = 0; j < 2; ++j){
            String nodeName = user.getChildNodes().item(j).getNodeName();

            switch(nodeName){
                case "name":
                    name = user.getChildNodes().item(j).getTextContent();
                    break;

                case "message":
                    message = user.getChildNodes().item(j).getTextContent();
                    break;

                default:
                    System.out.println("unknown attribute " + nodeName);
                    break;
            }
        }

        final String finalName = name;
        final String finalMessage = message;
        Platform.runLater(() -> ChatSceneView.getViewController().addItemChat(new MessageNode(finalName, finalMessage, my)));
    }
}
