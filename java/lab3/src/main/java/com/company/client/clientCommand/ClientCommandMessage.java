package com.company.client.clientCommand;

import com.company.XmlElement;
import com.company.client.ClientParser;
import com.company.client.RemoteServer;
import com.company.client.listenClientCommand.ListenAnswerOnMessage;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import org.w3c.dom.Document;

import java.util.ArrayDeque;
import java.util.Queue;

public class ClientCommandMessage extends ClientCommand {
    private ClientParser clientParser;

    public ClientCommandMessage(ClientParser clientParser){
        this.clientParser = clientParser;
    }

    @Override
    public Queue<SolvableTask> getQueueTask(RemoteServer remoteServer) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();
        String message = remoteServer.pollMessage();

        XmlElement xmlCommand = new XmlElement("command", "name", "message");
        XmlElement xmlMessage = new XmlElement("message", message);
        XmlElement xmlSession = new XmlElement("session", remoteServer.getId());
        xmlCommand.appendChild(xmlMessage);
        xmlCommand.appendChild(xmlSession);

        taskQueue.add(new SingleTask(xmlCommand.generateDocument()));

        clientParser.putWaitAnswer(new ListenAnswerOnMessage(remoteServer.getUsername(), message));
        return taskQueue;
    }
}
