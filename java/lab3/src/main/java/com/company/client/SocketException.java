package com.company.client;

public class SocketException extends Exception{
    public SocketException(String errMes){
        super(errMes);
    }
}
