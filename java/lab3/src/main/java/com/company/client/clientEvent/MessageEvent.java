package com.company.client.clientEvent;

import com.company.client.RemoteServer;
import com.company.client.clientCommand.ClientCommandFactory;
import com.view.ChatSceneView;

public class MessageEvent implements WaitMessage {
    private RemoteServer remoteServer;

    public MessageEvent(RemoteServer remoteServer){
        this.remoteServer = remoteServer;
        ChatSceneView.getViewController().addListenerMessage(this);
    }

    @Override
    public void updateMessage(String message) {
        String newMessage = message.replace("\n", "");
        String checkMessage = newMessage.replace(" ", "");

        if (!checkMessage.isEmpty()){
            remoteServer.putMessage(newMessage);
            remoteServer.putTask(ClientCommandFactory.getQueueTask(remoteServer, "message"));
        }
    }
}
