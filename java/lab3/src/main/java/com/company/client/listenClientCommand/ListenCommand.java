package com.company.client.listenClientCommand;

import org.w3c.dom.Document;

public abstract class ListenCommand {
    abstract public void react(Document document);
}
