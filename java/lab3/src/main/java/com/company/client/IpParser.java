package com.company.client;

import com.view.SceneController;

public class IpParser {
    private String ip;
    private Integer port;
    
    public void parseIp(String str){
        try{
            //str ~ 192.168.0.104:8080
            if (str == null){
                return;
            }

            String ipWithPort[] = str.split(":");

            if (ipWithPort.length != 2){
                throw new ParserException("ip error: ip must consist of two parts: the ip itself and the port;\nexample: 127.0.0.1:8080");
            }

            ip = ipWithPort[0];
            port = Integer.valueOf(ipWithPort[1]);

            checkIp(ip);
            checkPort(port);

        } catch(ParserException e){
            e.getLocalizedMessage();
            SceneController.getSceneController().showLoginErrorMessage("Ip error; example: 127.0.0.1:8080");
        } catch(Exception e){
            e.printStackTrace();
            SceneController.getSceneController().showLoginErrorMessage("Ip error; example: 127.0.0.1:8080");
        }
    }

    private void checkIp(String ip) throws ParserException {
        String arrayByteIp[] = ip.split(".");
        for (String num: arrayByteIp) {
            if (Integer.getInteger(num) < 0 || Integer.getInteger(num) > 255){
                throw new ParserException("ip error: each byte of the IP address must be in the range [0, 255]");
            }
        }
    }

    private static void checkPort(Integer port) throws ParserException {
        if (port < 0 || port > 65535){
            throw new ParserException("port error: the port must be in the range [0, 65535]");
        }
    }

    public String getIp() {
        return ip;
    }

    public Integer getPort() {
        return port;
    }
}
