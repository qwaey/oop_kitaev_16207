package com.company.client;

import com.view.SceneController;
import javafx.application.Platform;

import java.net.Socket;

public class ClientSender implements Runnable {
    private RemoteServer remoteServer;
    private ClientExecutor clientExecutor;

    ClientSender(Socket socket, RemoteServer remoteServer) {
        this.remoteServer = remoteServer;
        this.clientExecutor = new ClientExecutor(socket);
    }

    @Override
    public void run(){
        Thread.currentThread().setName("SENDER");
        while(!Thread.currentThread().isInterrupted()){
            try {
                remoteServer.waitTask();
                clientExecutor.execute(remoteServer);
            } catch (InterruptedException e) {
                return;
            } catch (SocketException e) {
                Platform.runLater(() -> SceneController.getSceneController().showLoginErrorMessage(e.getLocalizedMessage()));
                return;
            }
        }
    }
}