package com.company.client.clientCommand;

import com.company.client.RemoteServer;
import com.company.task.SolvableTask;
import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class ClientCommandFactory {
    private static Map<String, ClientCommand> commandMap = new HashMap<>();

    public static void addCommandInMap(String name, ClientCommand command){
        commandMap.put(name, command);
    }

    public static Queue<SolvableTask> getQueueTask(RemoteServer remoteServer, String commandName){
        return commandMap.get(commandName).getQueueTask(remoteServer);
    }
}
