package com.company.client.clientEvent;

import com.company.client.IpParser;
import com.company.client.ParserException;
import com.view.LoginSceneView;
import com.view.SceneController;

public class LoginEvent implements WaitUsername, WaitIpWithPort {
    private String ip;
    private Integer port;
    private String username;
    private IpParser ipParser = new IpParser();

    public LoginEvent(){
        LoginSceneView.getViewController().addListenerIpWithPort(this);
        LoginSceneView.getViewController().addListenerUsername(this);
    }

    @Override
    synchronized public void updateIpWithPort(String ipWithPort) {
        ipParser.parseIp(ipWithPort);
        this.ip = ipParser.getIp();
        this.port = ipParser.getPort();
        notify();
    }

    @Override
    synchronized public void updateUsername(String username) {
        try {
            if (username.isEmpty()) {
                throw new ParserException("Username can't be empty");
            }
        } catch(ParserException e) {
            e.getLocalizedMessage();
            SceneController.getSceneController().showLoginErrorMessage("Username can't be empty");
        }

        this.username = username;
        notify();
    }

    synchronized public String getIpSync() {
        while(ip == null){
            try {
                wait();
            } catch (InterruptedException e) {
                return null;
            }
        }

        return ip;
    }

    synchronized public Integer getPortSync() {
        while(port == null){
            try {
                wait();
            } catch (InterruptedException e) {
                return null;
            }
        }

        return port;
    }

    synchronized public void reset(){
        this.ip = null;
        this.port = null;
        this.username = null;
    }

    public String getUsername() {
        return username;
    }
}
