package com.company.client;

import com.company.client.clientCommand.*;
import com.company.client.clientEvent.LoginEvent;
import com.company.client.listenClientCommand.ListenAnswerOnUserlogin;
import com.company.client.listenClientCommand.ListenAnswerOnUserlogout;
import com.company.client.listenClientCommand.ListenAnswerOnUsermessage;
import com.company.listener.Listener;
import com.view.SceneController;
import javafx.application.Platform;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client implements Runnable {
    private Thread senderThread;
    private RemoteServer remoteServer;
    private LoginEvent loginEvent = new LoginEvent();
    private ClientParser clientParser;

    public Client(ClientParser clientParser){
        this.clientParser = clientParser;

        ClientCommandFactory.addCommandInMap("login", new ClientCommandLogin(clientParser));
        ClientCommandFactory.addCommandInMap("logout", new ClientCommandLogout(clientParser));
        ClientCommandFactory.addCommandInMap("message", new ClientCommandMessage(clientParser));
        ClientCommandFactory.addCommandInMap("list", new ClientCommandList(clientParser));
    }

    public void interruptUser(){
        if (senderThread == null){
            return;
        }

        remoteServer.putTask(ClientCommandFactory.getQueueTask(remoteServer, "logout"));
        senderThread.interrupt();
    }

    synchronized public void run() {
        System.out.println("Connect to " + loginEvent.getIpSync());
        Socket socket = connect();
        System.out.println("Successful connection");

        remoteServer = new RemoteServer(loginEvent.getUsername(), socket);

        Listener listener = new Listener(socket);
        listener.addListener(clientParser);

        clientParser.putEvent("userlogin", new ListenAnswerOnUserlogin());
        clientParser.putEvent("userlogout", new ListenAnswerOnUserlogout());
        clientParser.putEvent("message", new ListenAnswerOnUsermessage());

        remoteServer.putTask(ClientCommandFactory.getQueueTask(remoteServer, "login"));

        senderThread = new Thread(new ClientSender(socket, remoteServer));
        senderThread.start();
        (new Thread(listener)).start();
    }

    private Socket connect(){
        while(true){
            try {
                return new Socket(InetAddress.getByName(loginEvent.getIpSync()), loginEvent.getPortSync());
            } catch (IOException e) {
                loginEvent.reset();
                Platform.runLater(() -> SceneController.getSceneController().showLoginErrorMessage("Connection error. Please check ip and port"));
            }
        }
    }
}
