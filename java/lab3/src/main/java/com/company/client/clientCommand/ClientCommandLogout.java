package com.company.client.clientCommand;

import com.company.XmlElement;
import com.company.client.ClientParser;
import com.company.client.RemoteServer;
import com.company.client.listenClientCommand.ListenAnswerOnLogout;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;

import java.util.ArrayDeque;
import java.util.Queue;

public class ClientCommandLogout extends ClientCommand {
    private ClientParser clientParser;

    public ClientCommandLogout(ClientParser clientParser) {
        this.clientParser = clientParser;
    }

    @Override
    public Queue<SolvableTask> getQueueTask(RemoteServer remoteServer) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        XmlElement xmlCommand = new XmlElement("command", "name", "logout");
        XmlElement xmlSession = new XmlElement("session", remoteServer.getId());
        xmlCommand.appendChild(xmlSession);

        taskQueue.add(new SingleTask(xmlCommand.generateDocument()));

        clientParser.putWaitAnswer(new ListenAnswerOnLogout(remoteServer));
        return taskQueue;
    }
}