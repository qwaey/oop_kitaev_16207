package com.company.client.clientEvent;

import com.view.ChatSceneView;
import javafx.application.Platform;
import org.w3c.dom.Node;

public class OnlineBoardEvent {
    public static void addItem(Node user){
        // Cycle of the number of elements in the user
        int elements = user.getChildNodes().getLength();

        for(int j = 0; j < elements; ++j){
            String nodeName = user.getChildNodes().item(j).getNodeName();

            switch(nodeName){
                case "name":
                    final String name = user.getChildNodes().item(j).getTextContent();
                    Platform.runLater(() -> ChatSceneView.getViewController().addItem(name, new UserNode(name)));
                    break;

                case "type":
                    break;

                default:
                    System.out.println("unknown attribute " + nodeName);
                    break;
            }
        }
    }

    public static void removeItem(Node user){
        Platform.runLater(() -> ChatSceneView.getViewController().removeItem(user.getChildNodes().item(0).getTextContent()));
    }
}
