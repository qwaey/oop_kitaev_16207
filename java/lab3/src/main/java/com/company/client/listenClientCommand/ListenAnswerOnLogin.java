package com.company.client.listenClientCommand;

import com.company.client.RemoteServer;
import com.company.client.clientCommand.ClientCommandFactory;
import com.company.client.clientEvent.MessageEvent;
import com.view.SceneController;
import javafx.application.Platform;
import org.w3c.dom.Document;

public class ListenAnswerOnLogin extends ListenCommand {
    private RemoteServer remoteServer;

    public ListenAnswerOnLogin(RemoteServer remoteServer){
        this.remoteServer = remoteServer;
    }

    @Override
    public void react(Document document) {
        String answer = document.getDocumentElement().getTagName();

        switch(answer){
            case "success":
                String sessionId = document.getElementsByTagName("session").item(0).getTextContent();
                remoteServer.setSessionId(sessionId);
                remoteServer.putTask(ClientCommandFactory.getQueueTask(remoteServer, "list"));
                Platform.runLater(() -> {
                    SceneController.getSceneController().setSceneChat();
                    MessageEvent messageEvent = new MessageEvent(remoteServer);
                });
                break;

            case "error":
                String reason = document.getElementsByTagName("message").item(0).getTextContent();
                SceneController.getSceneController().showLoginErrorMessage("ERROR LOGIN; REASON: " + reason);
                break;

            default:
                System.out.println("PARS ERROR");
                break;
        }
    }
}
