package com.company.client.clientEvent;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

class UserNode extends BorderPane {
    UserNode(String name){
        Label label = new Label(name);
        label.setFont(Font.font("Roboto Medium", 24));
        setCenter(label);
    }
}