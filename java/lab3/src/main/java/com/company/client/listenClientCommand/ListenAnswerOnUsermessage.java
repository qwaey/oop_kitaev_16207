package com.company.client.listenClientCommand;

import com.company.client.clientEvent.OnlineMessageEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ListenAnswerOnUsermessage extends ListenCommand {
    @Override
    public void react(Document document) {
        Node user = document.getDocumentElement();
        OnlineMessageEvent.addItem(user, false);
    }
}