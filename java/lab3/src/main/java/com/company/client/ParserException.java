package com.company.client;

public class ParserException extends Exception{
    public ParserException(String errMes){
        super(errMes);
    }
}
