package com.company.client.clientEvent;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

class MessageNode extends BorderPane {
    MessageNode(String name, String message, boolean my){
        String newString = name + ":\n" + message;
        Label label = new Label(newString);
        label.setFont(Font.font("Roboto Medium", 28));

        if (my){
            setRight(label);
        } else {
            setLeft(label);
        }

        setMinHeight(80);
    }
}