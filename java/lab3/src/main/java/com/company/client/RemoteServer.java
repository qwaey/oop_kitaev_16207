package com.company.client;

import com.company.task.SingleTaskAdd;
import com.company.task.SolvableTask;
import com.view.SceneController;

import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Queue;

public class RemoteServer implements SingleTaskAdd, WaitingString {
    private String username;
    private String sessionId;
    private String chatClientName = "terrogram";
    private Socket socket;
    private Queue<SolvableTask> taskQueue = new ArrayDeque<>();
    private Queue<String> messageQueue = new ArrayDeque<>();

    public RemoteServer(String username, Socket socket) {
        this.socket = socket;
        this.username = username;
    }


    @Override
    synchronized public void putSingleTask(SolvableTask task) {
        taskQueue.add(task);
        notify();
    }


    // WORK WITH MY USERNAME
    public String getUsername() {
        if(username == null){
            SceneController.getSceneController().showLoginErrorMessage("Username must be not-null");
        }

        return username;
    }

    @Override
    public void update(String username) {
        this.username = username;
    }


    // WORK WITH CHAT CLIENT NAME
    public String getChatClientName() {
        return chatClientName;
    }


    // WORK WITH SESSION ID
    @Override
    public String getId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


    // WORK WITH MESSAGE QUEUE
    synchronized public void putMessage(String message){
        messageQueue.add(message);
    }

    synchronized public int queueSize(){
        return taskQueue.size();
    }

    synchronized public String pollMessage(){
        return messageQueue.poll();
    }

    synchronized public void waitTask() throws InterruptedException {
        if (Thread.currentThread().isInterrupted()){
            throw new InterruptedException();
        }

        while(taskQueue.size() == 0){
            wait();
        }
    }

    // WORK WITH TASK QUEUE
    synchronized public void putTask(Queue<SolvableTask> task) {
        if (task != null){
            taskQueue.addAll(task);
        }

        notify();
    }

    synchronized public SolvableTask getTask() {
        if (taskQueue.size() > 0){
            return taskQueue.poll();
        }

        return null;
    }

    // WORK WITH SOCKET
    synchronized public Socket getSocket() {
        return socket;
    }
}