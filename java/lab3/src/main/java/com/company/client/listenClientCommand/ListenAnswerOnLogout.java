package com.company.client.listenClientCommand;

import com.company.client.RemoteServer;
import org.w3c.dom.Document;

import java.io.IOException;

public class ListenAnswerOnLogout extends ListenCommand {
    private RemoteServer remoteServer;

    public ListenAnswerOnLogout(RemoteServer remoteServer){
        this.remoteServer = remoteServer;
    }

    @Override
    public void react(Document document){
        String answer = document.getDocumentElement().getTagName();

        switch(answer){
            case "success":
                try {
                    remoteServer.getSocket().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "error":
                String reason = document.getElementsByTagName("message").item(0).getTextContent();
                System.out.println("ERROR LOGOUT; REASON: " + reason);
                break;

            default:
                System.out.println("PARS ERROR");
                break;
        }
    }
}
