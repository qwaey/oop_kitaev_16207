package com.company.client.clientCommand;

import com.company.XmlElement;
import com.company.client.ClientParser;
import com.company.client.RemoteServer;
import com.company.client.listenClientCommand.ListenAnswerOnLogin;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;
import java.util.ArrayDeque;

import java.util.Queue;

public class ClientCommandLogin extends ClientCommand {
    private ClientParser clientParser;

    public ClientCommandLogin(ClientParser clientParser){
        this.clientParser = clientParser;
    }

    @Override
    public Queue<SolvableTask> getQueueTask(RemoteServer remoteServer) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        XmlElement xmlCommand = new XmlElement("command", "name", "login");
        XmlElement xmlUsername = new XmlElement("name", remoteServer.getUsername());
        XmlElement xmlChatClientName = new XmlElement("type", remoteServer.getChatClientName());
        xmlCommand.appendChild(xmlUsername);
        xmlCommand.appendChild(xmlChatClientName);

        taskQueue.add(new SingleTask(xmlCommand.generateDocument()));

        clientParser.putWaitAnswer(new ListenAnswerOnLogin(remoteServer));

        return taskQueue;
    }
}
