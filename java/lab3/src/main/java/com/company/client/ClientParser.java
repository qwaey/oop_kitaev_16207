package com.company.client;

import com.company.client.listenClientCommand.ListenCommand;
import com.company.listener.ListDoc;
import org.w3c.dom.Document;
import java.util.Map;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.HashMap;

public class ClientParser implements ListDoc {
    private Queue<ListenCommand> answerQueue = new ArrayDeque<>();
    private Map<String, ListenCommand> eventMap = new HashMap<>();

    synchronized public void putEvent(String commandName, ListenCommand listenCommand){
        eventMap.put(commandName, listenCommand);
    }

    synchronized public void putWaitAnswer(ListenCommand listenCommand){
        answerQueue.add(listenCommand);
    }

    @Override
    synchronized public void putDocument(Document document) {
        try {
            String commandName = document.getDocumentElement().getTagName();

            // Unexpected event (message, userlogin, userlogout)
            if (commandName.equals("event")){
                String eventName = document.getDocumentElement().getAttribute("name");
                eventMap.get(eventName).react(document);
                return;
            }

            if (answerQueue.size() == 0){
                throw new ParserException("answer document error: unexpected answer");
            }

            answerQueue.poll().react(document);

        } catch (ParserException e) {
            e.printStackTrace();
        }
    }
}
