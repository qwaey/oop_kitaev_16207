package com.company.client.listenClientCommand;

import com.company.XmlElement;
import com.company.client.clientEvent.OnlineMessageEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ListenAnswerOnMessage extends ListenCommand {
    private Document visualDoc;

    public ListenAnswerOnMessage(String name, String message){
        XmlElement event = new XmlElement("event", "name", "message");
        XmlElement username = new XmlElement("name", name);
        XmlElement mes = new XmlElement("message", message);
        event.appendChild(username);
        event.appendChild(mes);
        visualDoc = event.generateDocument();
    }

    @Override
    public void react(Document document) {
        String answer = document.getDocumentElement().getTagName();

        switch(answer){
            case "success":
                Node user = visualDoc.getDocumentElement();
                OnlineMessageEvent.addItem(user, true);
                break;

            case "error":
                String reason = document.getElementsByTagName("message").item(0).getTextContent();
                System.out.println("ERROR MESSAGE; REASON: " + reason);
                break;

            default:
                System.out.println("PARS ERROR");
                break;
        }
    }
}
