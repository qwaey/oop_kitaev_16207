package com.company.client.listenClientCommand;

import com.company.client.clientEvent.OnlineBoardEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ListenAnswerOnUserlogin extends ListenCommand {
    @Override
    public void react(Document document) {
        Node user = document.getDocumentElement();
        OnlineBoardEvent.addItem(user);
    }
}
