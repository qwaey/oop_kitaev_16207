package com.company.client.listenClientCommand;

import com.company.client.clientEvent.OnlineBoardEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ListenAnswerOnList extends ListenCommand {
    @Override
    public void react(Document document) {
        String answer = document.getDocumentElement().getTagName();

        switch(answer){
            case "success":
                int users = document.getDocumentElement().getElementsByTagName("listusers").item(0).getChildNodes().getLength();
                for(int i = 0; i < users; ++i){
                    Node user = document.getDocumentElement().getElementsByTagName("listusers").item(0).getChildNodes().item(i);
                    OnlineBoardEvent.addItem(user);
                }
                break;

            case "error":
                String reason = document.getElementsByTagName("message").item(0).getTextContent();
                System.out.println("ERROR LIST; REASON: " + reason);
                break;

            default:
                System.out.println("PARS ERROR");
                break;
        }
    }
}
