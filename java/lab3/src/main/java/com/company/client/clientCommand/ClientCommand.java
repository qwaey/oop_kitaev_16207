package com.company.client.clientCommand;

import com.company.client.RemoteServer;
import com.company.task.SolvableTask;
import org.w3c.dom.Document;
import java.util.Queue;

public abstract class ClientCommand {
    abstract public Queue<SolvableTask> getQueueTask(RemoteServer RemoteServer);
}
