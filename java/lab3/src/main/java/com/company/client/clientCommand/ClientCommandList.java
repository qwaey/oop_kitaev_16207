package com.company.client.clientCommand;

import com.company.XmlElement;
import com.company.client.ClientParser;
import com.company.client.RemoteServer;
import com.company.client.listenClientCommand.ListenAnswerOnList;
import com.company.task.SingleTask;
import com.company.task.SolvableTask;

import java.util.ArrayDeque;
import java.util.Queue;

public class ClientCommandList extends ClientCommand {
    private ClientParser clientParser;

    public ClientCommandList(ClientParser clientParser){
        this.clientParser = clientParser;
    }

    @Override
    public Queue<SolvableTask> getQueueTask(RemoteServer remoteServer) {
        Queue<SolvableTask> taskQueue = new ArrayDeque<>();

        XmlElement xmlCommand = new XmlElement("command", "name", "list");
        XmlElement xmlSession = new XmlElement("session", remoteServer.getId());
        xmlCommand.appendChild(xmlSession);

        taskQueue.add(new SingleTask(xmlCommand.generateDocument()));

        clientParser.putWaitAnswer(new ListenAnswerOnList());
        return taskQueue;
    }
}
