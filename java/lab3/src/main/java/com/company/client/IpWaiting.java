package com.company.client;

public class IpWaiting implements WaitingString {
    private String ipWithPort;

    @Override
    synchronized public void update(String string) {
        this.ipWithPort = string;
        notify();
    }

    synchronized public String getIpWithPort(){
        while(ipWithPort == null){
            try {
                wait();
            } catch (InterruptedException e) {
                return null;
            }
        }

        return ipWithPort;
    }
}
