package com.company.client;

import java.io.ByteArrayOutputStream;
import java.net.Socket;
import java.io.*;

public class ClientExecutor {
    private Socket socket;

    ClientExecutor(Socket socket) {
        this.socket = socket;
    }

    public void execute(RemoteServer remoteServer) throws SocketException {
        try {
            if (socket.isClosed()){
                Thread.currentThread().interrupt();
                return;
            }

            DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
            ByteArrayOutputStream byteOutputMessage = new ByteArrayOutputStream();

            while(remoteServer.queueSize() > 0){
                remoteServer.getTask().resolve(writer, byteOutputMessage);
            }
        } catch (IOException e) {
            throw new SocketException("Remote server not responding :(");
        }
    }
}
