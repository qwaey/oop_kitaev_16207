package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.*;

public class XmlElement {
    private String type;
    private String attributeKey;
    private String attributeValue;
    private String textContent;
    private Queue<XmlElement> childList = new ArrayDeque<>();

    public XmlElement(String type){
        this.type = type;
    }

    public XmlElement(String type, String attributeKey, String attributeValue){
        this.type = type;
        this.attributeKey = attributeKey;
        this.attributeValue = attributeValue;
    }

    public XmlElement(String type, String textContent){
        this.type = type;
        this.textContent = textContent;
    }

    public void appendChild(XmlElement element){
        childList.add(element);
    }

    public Document generateDocument(){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            Document document = factory.newDocumentBuilder().newDocument();
            document.appendChild(generateElement(document));
            return document;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Element generateElement(Document document){
        Element element = document.createElement(type);

        if (attributeKey != null && attributeValue != null){
            element.setAttribute(attributeKey, attributeValue);
        }

        if (textContent != null){
            element.setTextContent(textContent);
        }

        childList.forEach(iter -> element.appendChild(iter.generateElement(document)));
        return element;
    }
}
