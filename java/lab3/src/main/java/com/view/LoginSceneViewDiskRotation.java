package com.view;

import javafx.application.Platform;

public class LoginSceneViewDiskRotation extends Thread {
    public void run(){
        Thread.currentThread().setName("DISKER");
        while(!Thread.currentThread().isInterrupted()){
            try {
                Platform.runLater(() -> LoginSceneView.getViewController().rotateDisk(0.5));
                sleep(10);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}