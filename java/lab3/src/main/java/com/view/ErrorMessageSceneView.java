package com.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class ErrorMessageSceneView implements Initializable {
    static private ErrorMessageSceneView errorMessageSceneView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessageSceneView = this;
    }

    public static ErrorMessageSceneView getViewController() {
        return errorMessageSceneView;
    }

    @FXML private Text errorMessage;
    public void setErrorMessage(String errorMessage) {
        this.errorMessage.setText(errorMessage);
    }
}