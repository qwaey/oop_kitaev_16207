package com.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class SceneController {
    private Stage primaryStage;
    private static SceneController sceneController;

    public SceneController(Stage primaryStage){
        this.primaryStage = primaryStage;
        sceneController = this;
    }

    public static SceneController getSceneController(){
        return sceneController;
    }

    public void setSceneLogin(){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/scene.fxml"));
            Scene scene = new Scene(root, 1280, 720);

            primaryStage.setResizable(false);
            primaryStage.setTitle("Terrogram");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setSceneChat(){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/scene2.fxml"));
            Scene scene = new Scene(root, 1280, 720);

            primaryStage.setResizable(false);
            primaryStage.setTitle("Terrogram");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showLoginErrorMessage(String errMes){
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/loginErr.fxml"));
            Scene scene = new Scene(root, 480, 120);

            ErrorMessageSceneView.getViewController().setErrorMessage(errMes);

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}