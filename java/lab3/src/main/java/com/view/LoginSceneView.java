package com.view;

import com.company.client.clientEvent.WaitIpWithPort;
import com.company.client.clientEvent.WaitUsername;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class LoginSceneView implements Initializable {
    static private LoginSceneView loginSceneView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginSceneView = this;
        setContainersStyle();
    }

    public static LoginSceneView getViewController() {
        return loginSceneView;
    }

    // DISK ROTATION
    @FXML private ImageView leftDisk;
    @FXML private ImageView rightDisk;
    @FXML private double direction = 360.0;

    synchronized public void rotateDisk(double angle){
        direction -= angle;
        if (direction < 0.0){
            direction += 360.0;
        }

        leftDisk.setRotate(direction);
        rightDisk.setRotate(direction);
    }


    // CONTAINERS SETTING
    @FXML private TextField loginField;
    @FXML private TextField loginFieldIp;
    @FXML private Button enterButton;

    private void setContainersStyle(){
        Paint paintBorder = new Color(1,1,1, 0.3);

        CornerRadii cornerRadiiLoginField = new CornerRadii(8);
        CornerRadii cornerRadiiEnterButton = new CornerRadii(4);
        BorderWidths borderWidths = new BorderWidths(2);

        Border borderLoginField = new Border(new BorderStroke(paintBorder, BorderStrokeStyle.SOLID, cornerRadiiLoginField, borderWidths));
        Border borderEnterButton = new Border(new BorderStroke(paintBorder, BorderStrokeStyle.SOLID, cornerRadiiEnterButton, borderWidths));

        loginField.setStyle("-fx-text-fill: white");
        loginField.setAlignment(Pos.CENTER);
        loginField.setBackground(Background.EMPTY);
        loginField.setBorder(borderLoginField);

        loginFieldIp.setStyle("-fx-text-fill: white");
        loginFieldIp.setAlignment(Pos.CENTER);
        loginFieldIp.setBackground(Background.EMPTY);
        loginFieldIp.setBorder(borderLoginField);

        enterButton.setStyle("-fx-text-fill: white");
        enterButton.setBackground(Background.EMPTY);
        enterButton.setBorder(borderEnterButton);
    }


    // CHECK ENTER EVENT
    private List<WaitUsername> subscribersUsername = new ArrayList<>();
    synchronized public void addListenerUsername(WaitUsername subscriber){
        subscribersUsername.add(subscriber);
    }

    private List<WaitIpWithPort> subscribersIpWithPort = new ArrayList<>();
    synchronized public void addListenerIpWithPort(WaitIpWithPort subscriber){
        subscribersIpWithPort.add(subscriber);
    }

    synchronized public void login(){
        subscribersUsername.forEach(sub -> sub.updateUsername(loginField.getText()));
        subscribersIpWithPort.forEach(sub -> sub.updateIpWithPort(loginFieldIp.getText()));
    }

    public void loginOnEnter(KeyEvent event){
        if (event.getCode() == KeyCode.ENTER){
            login();
        }
    }
}