package com.view;

import com.company.client.clientEvent.WaitMessage;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import java.util.Map;
import java.util.HashMap;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ChatSceneView implements Initializable {
    static private ChatSceneView chatSceneView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chatSceneView = this;
    }

    public static ChatSceneView getViewController() {
        return chatSceneView;
    }

    // CONTAINERS SETTING
    @FXML private TextArea messageArea;

    // CHECK ENTER EVENT
    private List<WaitMessage> subscribersMessage = new ArrayList<>();
    synchronized public void addListenerMessage(WaitMessage subscriber){
        subscribersMessage.add(subscriber);
    }

    synchronized public void sendMessage(KeyEvent event){
        if (event.getCode() != KeyCode.ENTER){
            return;
        }

        subscribersMessage.forEach(sub -> sub.updateMessage(messageArea.getText()));
        messageArea.setText("");
    }

    // WORK WITH ONLINE PANE
    @FXML private VBox vBox;
    private Map<String, BorderPane> paneMap = new HashMap<>();
    synchronized public void addItem(String name, BorderPane pane){
        paneMap.put(name, pane);
        vBox.getChildren().add(pane);
    }

    synchronized public void removeItem(String name){
        vBox.getChildren().remove(paneMap.get(name));
        paneMap.remove(name);
    }

    // WORK WITH MESSAGE LIST
    @FXML private VBox chatVBox;
    @FXML private ScrollPane messageScrollPane;
    synchronized public void addItemChat(BorderPane pane){
        chatVBox.getChildren().add(pane);
        messageScrollPane.setVvalue(1.0d);
    }
}