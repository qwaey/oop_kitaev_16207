package com.lab1.java;

public class lab1 {
    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Please call: lab1 <filename>\n");
            return;
        }

        String FILENAME = args[0];

        FileReader reader = new FileReader();
        FileWriter writer = new FileWriter();
        Sorter sorter = new Sorter();

        sorter.sort(reader.read(FILENAME));
        writer.write(FILENAME, sorter.getSet(), reader.getSum());
    }
}