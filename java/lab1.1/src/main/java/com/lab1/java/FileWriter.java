package com.lab1.java;

import java.io.*;
import java.util.Set;

public class FileWriter {
    public void write(String filename, Set<WordPair> set, long sum){
        final String newFilename = filename + ".csv";
        final StringBuilder strBuild = new StringBuilder();

        try (Writer out = new OutputStreamWriter(new FileOutputStream(newFilename))) {
            strBuild.append("Source:;").append(filename);
            out.write(strBuild.toString());
            strBuild.setLength(0);

            for (WordPair iter : set) {
                strBuild.append("\n").append(iter.getKey()).append(";").append(iter.getValue()).append(";").append(((float)iter.getValue() / sum * 100)).append("%");
                out.write(strBuild.toString());
                strBuild.setLength(0);
            }
        }
        catch(FileNotFoundException e){
            System.err.println("File with name: " + e.getLocalizedMessage() + " not found");
        }
        catch(IOException e){
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
    }
}