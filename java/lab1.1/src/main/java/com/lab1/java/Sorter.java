package com.lab1.java;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Sorter {
    private Set<WordPair> treeset = null;

    Sorter(){
        this.treeset = new TreeSet<>(new WordPairComparator());
    }

    public void sort(Map<String, WordPair> map) {
        treeset.addAll(map.values());
    }

    public Set<WordPair> getSet(){
        return treeset;
    }
}