package com.lab1.java;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Character.isLetterOrDigit;

public class FileReader {
    private Map<String, WordPair> map = null;
    private long sum = 0;

    FileReader() {
        this.map = new HashMap<>();
    }

    public long getSum(){
        return sum;
    }

    private void insertBuffer(final StringBuilder strBuild, final Map<String, WordPair> map, final char bufChar){

        if (isLetterOrDigit(bufChar)){
            strBuild.append(bufChar);
            return;
        }

        if (!strBuild.toString().isEmpty()) {
            map.merge(strBuild.toString(), new WordPair(strBuild.toString(), 1), WordPair::sum);
            sum++;
            strBuild.setLength(0);
        }
    }

    public Map<String, WordPair> read(String filename) {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();

        try (Reader in = new InputStreamReader(new FileInputStream(filename))) {

            for(;;) {
                int rsz = in.read(buffer, 0, buffer.length);

                if (rsz < 0) {
                    insertBuffer(out, map, ' ');
                    break;
                }

                for (int i = 0; i < rsz; i++){
                    insertBuffer(out, map, buffer[i]);
                }
            }
        }
        catch(FileNotFoundException e){
            System.err.println("File with name: " + e.getLocalizedMessage() + " not found");
        }
        catch(IOException e){
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }

        return map;
    }
}