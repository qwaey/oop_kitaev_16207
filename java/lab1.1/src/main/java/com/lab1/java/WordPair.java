package com.lab1.java;

import java.util.Comparator;

class WordPair {
    private String key = "";
    private Integer value = 1;

    WordPair(String key){
        this.key = key;
    }

    WordPair(String key, Integer value){
        this.key = key;
        this.value = value;
    }

    public int hashCode(){
        return key.hashCode();
    }

    public boolean equals(WordPair obj){
        return (this.key.equals(obj.key));
    }

    /*public void setValue(Integer obj){
        value = obj;
    }*/

    public String getKey(){
        return key;
    }

    public Integer getValue(){
        return value;
    }

    public static WordPair sum(WordPair o1, WordPair o2){
        return new WordPair(o1.key, o1.value + o2.value);
    }
}

class WordPairComparator implements Comparator<WordPair> {
    @Override
    public int compare(WordPair o1, WordPair o2) {
        return (o1.getValue() <= o2.getValue() ? 1 : -1);
    }
}
