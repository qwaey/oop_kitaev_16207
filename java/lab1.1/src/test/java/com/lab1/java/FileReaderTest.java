package com.lab1.java;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class FileReaderTest {

    @Test
    public void read() {
        String tempFilename = "test_file.tmp";
        StringBuilder strBuild = new StringBuilder();

        try(Writer out = new OutputStreamWriter(new FileOutputStream(tempFilename))){
            strBuild.append("hello ||| / hello world hello   123456   ,"); // 5 word
            out.write(strBuild.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileReader reader = new FileReader();
        reader.read(tempFilename);

        assertEquals(reader.getSum(), 5);
    }
}